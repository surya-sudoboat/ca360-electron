const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;
const server = require('./ca360-server/app.js');
if(require('electron-squirrel-startup')) return;

const path = require('path');
const url = require('url');

let mainWindow;

function createWindow () {
    mainWindow = new BrowserWindow();
    // mainWindow.setIcon(path.join(__dirname ,'/app/assets/CA360-logos.jpeg'))
    mainWindow.maximize()
 
    mainWindow.webContents.openDevTools()


    mainWindow.loadURL('http://localhost:9000/');
    
    mainWindow.on('closed', function () {
      
        mainWindow = null;
    });
}


app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', function () {
   
    if (mainWindow === null) {
        createWindow();
    }
});


