import axios from 'axios';
import { getCookie } from "../cookie/Index";

// Instance For Axios Request
const instance = axios.create({
  baseURL: 'https://enigmatic-chamber-52575.herokuapp.com/api',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
});

// Axios Get Request

const getData = (url, param) => {
  const token = getCookie("token");

  return instance({
    headers: { Authorization: `Bearer ${token}`, ...instance.defaults.headers },
    method: 'GET',
    url,
    params: param,
  });
};

// Axios Post Request

const postData = (data, url) => {
  const token = getCookie("token");

  return instance({
    headers: { Authorization: `Bearer ${token}`, ...instance.defaults.headers },
    method: 'POST',
    url,
    data,
  });
};

// Axios Put Request

const putData = (data, url) => {
  const token = getCookie("token");
  return instance({
    headers: { Authorization: `Bearer ${token}`, ...instance.defaults.headers },
    method: 'PUT',
    url,
    data,
  });
};

// Axios Delete Request for Delete

const deleteData = (url, data) => {
  const token = getCookie("token");
  return instance({
    headers: { Authorization: `Bearer ${token}`, ...instance.defaults.headers },
    method: 'DELETE',
    data,
    url,
  });
};

export {
  getData, postData, putData, deleteData,
};
