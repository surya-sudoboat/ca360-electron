import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Grid, Box, Typography, Button, FormControl, Select, MenuItem, Checkbox, ListItemText, Radio,
  RadioGroup, FormControlLabel,
} from '@material-ui/core';
import CircularProgress from '@mui/material/CircularProgress';
import ArrowRightAlt from '@mui/icons-material/ArrowRightAlt';
// import Divider from '@mui/material/Divider';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Tinput, DropdownInput } from './Inputs';
import { ChangeTab, saveClinetInfo } from '../../../actions/index';
import { getData } from '../../Network/Index';

// Styling of dropdown of multiple selection Input
const MenuProps = {
  getContentAnchorEl: null,
  PaperProps: {
    style: {
      maxHeight: 250,
      width: 220,
      marginTop: 60,
    },
  },
};

function Form() {
  // Varibles Used in Form
  const initialValues = useSelector((state) => state.NewClientData);
  // Variable for storing form data.
  const [values, setValues] = useState(initialValues);
  // Variable to check error
  const [AadharError, setAadharError] = useState(false);
  const [PrimaryMobileError, setPrimaryMobileError] = useState(false);
  const [SecondaryMobileError, setSecondaryMobileError] = useState(false);
  const [options, setOptions] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  // Variable to use dispatch
  const dispatch = useDispatch();
  // Variable for styling
  // const btnStyle = { padding: 10, margin: "15px 0px" };
  // Styling for loader
  const loaderStyle = { color: "white" };
  // Function to all the dropdown values

  useEffect(() => {
    const url = `/enums`;
    getData(url)
      .then((res) => {
        console.log(res);
        setOptions(res.data.data);
        // eslint-disable-next-line no-plusplus
        for (let i = 0; i < res.data.data.entityType.length; i++) {
          if (res.data.data.entityType[i] === "individual") {
            res.data.data.entityType[i] = "Individual";
          } else if (res.data.data.entityType[i] === "firm") {
            res.data.data.entityType[i] = "Firm";
          } else if (res.data.data.entityType[i] === "company") {
            res.data.data.entityType[i] = "Company";
          } else {
            // eslint-disable-next-line no-continue
            continue;
          }
        }
        setIsLoading(false);
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  }, []);

  // Function to handle the change of Inputs enter by userr

  const handleInputChange = (e, value, name) => {
    value = e.target.value;
    name = e.target.name;
    if (name === 'aadhaar') {
      if (value.length === 12) {
        setAadharError(false);
      } else {
        setAadharError(true);
      }
    }
    if (name === 'contacts.primaryMobile') {
      if (value.length === 14) {
        setPrimaryMobileError(false);
      } else {
        setPrimaryMobileError(true);
      }
    }
    if (name === 'contacts.secondaryMobile') {
      if (value.length === 14) {
        setSecondaryMobileError(false);
      } else {
        setSecondaryMobileError(true);
      }
    }
    // if (name === 'dateOfBirth') {
    //   const today = new Date();
    //   const birthDate = new Date(value);
    //   let age = today.getFullYear() - birthDate.getFullYear();
    //   const m = today.getMonth() - birthDate.getMonth();
    //   if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    //     age -= 1;
    //   }
    //   if (age >= 18) {
    //     setDobError(false);
    //   } else {
    //     setDobError(true);
    //   }
    // }
    const str = name.split('.');
    if (str.length === 2) {
      const newValues = {
        ...values,
        [str[0]]: { ...values[str[0]], [str[1]]: value },
      };
      setValues(newValues);
      dispatch(saveClinetInfo(newValues));
    } else {
      const newValues = { ...values, [name]: value };
      setValues(newValues);
      dispatch(saveClinetInfo(newValues));
    }
  };

  // Function to handle Chnage of Tab Of Client   Page

  const tabChange = (e) => {
    e.preventDefault();
    if (AadharError || PrimaryMobileError || SecondaryMobileError) {
      toast.error('Please fill Valid Details');
      dispatch(saveClinetInfo(values));
      return;
    }
    // if (dobError) {
    //   toast.error('Age of Client sould be minimum 18 years');
    //   return;
    // }
    dispatch(ChangeTab('2'));
  };

  // If The view Tab of client Page is of for basic Details
  return (
    <Grid>
      {/* Wait Untill all info fetches by apis */}
      {!isLoading ? (
      // Form to enter the basic details of Client
        <Box>
          <form onSubmit={tabChange}>
            <Grid container justifyContent="space-around" spacing={2}>
              {/* Inputs with different lables and values and more attributes */}
              <Tinput
                lable="Business Name"
                value={values.businessName}
                handleInputChange={handleInputChange}
                name="businessName"
              />
              <Tinput
                lable="Responsible Person Name"
                value={values.dependent}
                handleInputChange={handleInputChange}
                name="dependent"
              />
              <Tinput
                lable="DOB / DOI"
                value={values.dateOfBirth}
                handleInputChange={handleInputChange}
                type="date"
                name="dateOfBirth"
              />
              <Tinput
                lable="Date of Commencement"
                value={values.startDate}
                handleInputChange={handleInputChange}
                type="date"
                name="startDate"
              />
              <Tinput
                lable="Aadhaar No."
                value={values.aadhaar}
                handleInputChange={handleInputChange}
                name="aadhaar"
                isError={AadharError}
              />
              <DropdownInput
                option={options.entityType}
                value={values.entityType}
                handleInputChange={handleInputChange}
                lable="Entity Status"
                name="entityType"
              />
              {/* Input for multiple selection */}
              <Grid xs={6} item>
                <Typography>
                  Nature of Business
                  <span className="redColor">*</span>
                </Typography>
                <FormControl fullWidth size="small">
                  <Select
                    multiple
                    name="natureOfBusiness"
                    value={values.natureOfBusiness}
                    onChange={handleInputChange}
                    renderValue={(selected) => selected.join(', ')}
                    MenuProps={MenuProps}
                    variant="outlined"
                    required
                  >
                    {options.natureOfBusiness.map((name) => (
                      <MenuItem key={name} value={name}>
                        <Checkbox
                          checked={values.natureOfBusiness.indexOf(name) > -1}
                        />
                        <ListItemText primary={name} />
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Tinput
                lable="Notes"
                value={values.notes}
                handleInputChange={handleInputChange}
                name="notes"
              />
              <Tinput
                lable="Primary Mobile No"
                value={values.contacts.primaryMobile}
                handleInputChange={handleInputChange}
                name="contacts.primaryMobile"
                isError={PrimaryMobileError}
              />
              <Tinput
                lable="Secondary Mobile No"
                value={values.contacts.secondaryMobile}
                handleInputChange={handleInputChange}
                name="contacts.secondaryMobile"
                isError={SecondaryMobileError}
              />
              <Tinput
                lable="Primary Mail ID"
                value={values.contacts.primaryMail}
                handleInputChange={handleInputChange}
                type="email"
                name="contacts.primaryMail"
              />
              <Tinput
                lable="Secondary Mail ID"
                value={values.contacts.secondaryMail}
                handleInputChange={handleInputChange}
                type="email"
                name="contacts.secondaryMail"
              />
              <Tinput
                lable="Address"
                value={values.contacts.address}
                multiline
                handleInputChange={handleInputChange}
                name="contacts.address"
              />
              <DropdownInput
                option={options.whatsappGroupType}
                value={values.contacts.whatsappGroupType}
                handleInputChange={handleInputChange}
                lable="WhatsApp Group"
                name="contacts.whatsappGroupType"
              />
              {/* Input Of Radio Button */}
              <Grid xs={12} item>
                <Typography>
                  WhatsApp To
                  <span className="redColor">*</span>
                </Typography>
                <RadioGroup
                  onChange={handleInputChange}
                  value={values.contacts.whatsappTo}
                  name="contacts.whatsappTo"
                  defaultValue="Primary Mobile No"
                >
                  <FormControlLabel
                    value={values.contacts.primaryMobile}
                    control={<Radio required />}
                    label="Primary Mobile No"
                  />
                  <FormControlLabel
                    value={values.contacts.secondaryMobile}
                    control={<Radio required />}
                    label="Secondary Mobile No"
                  />
                  <FormControlLabel
                    value={`${values.contacts.primaryMobile}, ${values.contacts.secondaryMobile}`}
                    control={<Radio required />}
                    label="Both"
                  />
                </RadioGroup>
              </Grid>
              <Grid xs={9} item />
              <Grid xs={3} item>
                <Button
                  type="submit"
                  variant="contained"
                  style={{
                    background: "#8A2BE2", color: "white", textTransform: "none", padding: "2px 20px",
                  }}
                >
                  Next
                  <ArrowRightAlt fontSize="large" style={{ marginLeft: "10px" }} />
                </Button>
              </Grid>
            </Grid>
          </form>
        </Box>
      ) : (
        <div className="rightLoader">
          <CircularProgress size={50} style={loaderStyle} />
        </div>
      )}
      <ToastContainer />
    </Grid>
  );
}

export default Form;
