/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  Grid, Typography, Button, TextField, Box, FormControl,
} from '@material-ui/core';
import CircularProgress from '@mui/material/CircularProgress';
import Autocomplete from '@mui/material/Autocomplete';
import Divider from '@mui/material/Divider';
import { ToastContainer, toast } from 'react-toastify';
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import { withStyles } from "@material-ui/core/styles";
import { Tinput, DropdownInput, RowHeading } from './Inputs';
import {
  ChangeTab, resetClinetInfo, viewClient, saveClinetInfo, reload, clientIDchange,
} from '../../../actions/index';
import { getData, postData, putData } from '../../Network/Index';

const StyledAutocomplete = withStyles({
  inputRoot: {
    '&&[class*="MuiOutlinedInput-root"] $input': {
      padding: 3,
    },
  },
  input: {},
})(Autocomplete);

function Form({ mode }) {
  // Varibles Used in Form
  const initialValues = useSelector((state) => state.NewClientData);
  const clientID = useSelector((state) => state.clientIDchange);
  // Variable for storing form data.
  const [values, setValues] = useState(initialValues);
  const [options, setOptions] = useState({});
  const [isLoading, setIsLoading] = useState(true);
  const [gstState, setGstState] = useState(false);
  // Variable to use dispatch
  const dispatch = useDispatch();
  // Variable for styling
  const btnStyle = {
    padding: 10, margin: "15px auto", background: "#8A2BE2",
  };
  // Styling for loader
  const loaderStyle = { color: "white" };

  // Function to all the dropdown values

  useEffect(() => {
    const url = `/enums`;
    getData(url)
      .then((res) => {
        setOptions(res.data.data);
        setIsLoading(false);
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
    if (mode === 'edit' && values.gst !== undefined) {
      setGstState(true);
    }
  }, []);

  // Function to handle the change of Inputs enter by userr

  const handleInputChange = (e, value, name) => {
    if (value === undefined || name === undefined) {
      value = e.target.value;
      name = e.target.name;
    }
    const str = name.split('.');
    if (str.length === 2) {
      const newValues = {
        ...values,
        [str[0]]: { ...values[str[0]], [str[1]]: value },
      };
      setValues(newValues);
      dispatch(saveClinetInfo(newValues));
    } else {
      const newValues = { ...values, [name]: value };
      setValues(newValues);
      dispatch(saveClinetInfo(newValues));
    }
  };

  // Function to handle the Submit of Client Form and also to Update the Client Information

  const HandleSubmit = (e) => {
    e.preventDefault();
    if (mode === "create") {
      if (gstState === false) {
        delete values.gst;
      }
      const url = `/client`;
      // Posting the Client Information to Server
      postData(values, url)
        .then((res) => {
          toast.success(res.data.message);
          dispatch(resetClinetInfo());
          // dispatch(clientIDchange(res.data.data._id));
          dispatch(viewClient());
          dispatch(ChangeTab('1'));
          dispatch(reload());
        })
        .catch((error) => {
          toast.error(error.response.data.message);
          dispatch(saveClinetInfo(values));
        });
    } else {
      const url = `/client/${clientID}`;
      // Updating the Client Information to Server
      putData(JSON.stringify(values), url)
        .then((res) => {
          toast.success(res.data.message);
          dispatch(resetClinetInfo());
          dispatch(viewClient());
          dispatch(ChangeTab('1'));
          dispatch(reload());
        })
        .catch((error) => {
          toast.error(error.response.data.message);
          dispatch(saveClinetInfo(values));
        });
    }
  };
  return (
    <Grid>
      {/* Wait Untill api response */}
      {!isLoading ? (
        // Form to enter the login details of Client
        <Box>
          <form onSubmit={HandleSubmit}>
            <Grid container justifyContent="space-around" spacing={1}>
              {/* Form Heading */}
              <RowHeading lable="Income Tax" />
              {/* Different Inputs with different Lable and various attributes */}

              <Tinput
                lable="Username-IT"
                value={values.it.username}
                handleInputChange={handleInputChange}
                name="it.username"
              />
              <Tinput
                lable="Password-IT"
                value={values.it.password}
                handleInputChange={handleInputChange}
                name="it.password"
              />
              <RowHeading lable="TDS" />
              <Tinput
                lable="TAN"
                value={values.tds.tanNumber}
                handleInputChange={handleInputChange}
                name="tds.tanNumber"
              />
              <Grid xs={6} item>
                {' '}
              </Grid>
              <Tinput
                lable="Username-IT Portal"
                value={values.tds.itUsername}
                handleInputChange={handleInputChange}
                name="tds.itUsername"
              />
              <Tinput
                lable="Password-IT Portal"
                value={values.tds.itPassword}
                handleInputChange={handleInputChange}
                name="tds.itPassword"
              />
              <Tinput
                lable="Username-Traces"
                value={values.tds.tracesUsername}
                handleInputChange={handleInputChange}
                name="tds.tracesUsername"
              />
              <Tinput
                lable="Password-Traces"
                value={values.tds.tracesPassword}
                handleInputChange={handleInputChange}
                name="tds.tracesPassword"
              />
              { mode === 'create' && (
              <Grid xs={12} item>
                <FormControlLabel
                  control={(
                    <Switch
                      checked={gstState}
                      onChange={(e) => { setGstState(e.target.checked); }}
                    />
              )}
                  label="Fill GST Details"
                />
              </Grid>
              )}
              {gstState && (
              <>
                <RowHeading lable="GST" />
                <Tinput
                  lable="GSTIN"
                  value={values.gst.gstNumber}
                  handleInputChange={handleInputChange}
                  name="gst.gstNumber"
                />
                <Grid xs={6} item>
                  {' '}
                </Grid>
                <Tinput
                  lable="Username-GST"
                  value={values.gst.gstUsername}
                  handleInputChange={handleInputChange}
                  name="gst.gstUsername"
                />
                <Tinput
                  lable="Password-GST"
                  value={values.gst.gstPassword}
                  handleInputChange={handleInputChange}
                  name="gst.gstPassword"
                />
                <Tinput
                  lable="Username-E-way Bill"
                  value={values.gst.ewayUsername}
                  handleInputChange={handleInputChange}
                  name="gst.ewayUsername"
                />
                <Tinput
                  lable="Password-E-Way Bill"
                  value={values.gst.ewayPassword}
                  handleInputChange={handleInputChange}
                  name="gst.ewayPassword"
                />
                <Grid xs={6} item>
                  <Typography>
                    Registered State
                    <span className="redColor">*</span>
                  </Typography>
                  <StyledAutocomplete
                    options={options.registeredState}
                    isOptionEqualToValue={(option, value) => option.id === value.id}
                    value={values.gst.registeredState}
                    onInputChange={(e, val) => handleInputChange(e, val, 'gst.registeredState')}
                    ListboxProps={{ style: { maxHeight: '180px' } }}
                    renderInput={(params) => (
                      <TextField {...params} size="small" required variant="outlined" />
                    )}
                  />
                </Grid>
                <DropdownInput
                  option={options.paymentMode}
                  value={values.gst.paymentMode}
                  handleInputChange={handleInputChange}
                  lable="Payment Mode"
                  name="gst.paymentMode"
                />
                <DropdownInput
                  option={options.verificationCode}
                  value={values.gst.verificationCode}
                  handleInputChange={handleInputChange}
                  lable="DSC / EVC"
                  name="gst.verificationCode"
                />
                <DropdownInput
                  option={options.filingType}
                  value={values.gst.filingType}
                  handleInputChange={handleInputChange}
                  lable="Filling Type"
                  name="gst.filingType"
                />
              </>
              )}
              {mode === "create" ? (
                <Grid xs={12} item>
                  <Button
                    type="submit"
                    color="primary"
                    variant="contained"
                    style={{
                      margin: '50px auto 20px auto', padding: "4px 20px", backgroundColor: "#8A2BE2", position: "relative", left: "50%", transform: "translateX(-50%)",
                    }}
                  >
                    Submit
                  </Button>
                </Grid>
              ) : (
                <Grid xs={12} item>
                  <Button
                    type="submit"
                    color="primary"
                    variant="contained"
                    style={{
                      margin: '50px auto 20px auto', padding: "4px 20px", backgroundColor: "#8A2BE2", position: "relative", left: "50%", transform: "translateX(-50%)",
                    }}
                  >
                    Update
                  </Button>
                </Grid>
              )}
            </Grid>
          </form>
        </Box>
      ) : (
        <div className="rightLoader">
          <CircularProgress size={50} style={loaderStyle} />
        </div>
      )}
      <ToastContainer />
    </Grid>
  );
}

export default Form;
