import React from 'react';
import {
  TextField,
  Typography,
  Grid,
  Select,
  MenuItem,
  FormControl,
  Box,
} from '@material-ui/core';

// Styling of Dropdown menu
const MenuProps = {
  getContentAnchorEl: null,
  PaperProps: {
    style: {
      maxHeight: 200,
      width: 220,
      marginTop: 60,
    },
  },
};

// For Dropdown Inputs

function DropdownInput({
  lable, option, value, handleInputChange, name,
}) {
  return (
    <Grid xs={6} item>
      <Typography>
        {lable}
        <span className="redColor">*</span>
      </Typography>
      <FormControl fullWidth size="small">
        <Select
          variant="outlined"
          value={value}
          onChange={handleInputChange}
          name={name}
          MenuProps={MenuProps}
          required
        >
          {option.map((item) => (
            <MenuItem value={item} key={item}>
              {item}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Grid>
  );
}

// For Text Inputs

function Tinput({
  lable,
  value,
  handleInputChange,
  type,
  name,
  isError,
  text,
}) {
  return (
    <Grid xs={6} item>
      <Typography noWrap>
        {lable}
        <span className="redColor">*</span>
      </Typography>
      <TextField
        variant="outlined"
        size="small"
        maxRows={4}
        value={value}
        onChange={handleInputChange}
        name={name}
        fullWidth
        required
        type={type}
        error={isError}
        helperText={text}
      />
    </Grid>
  );
}

// To make row for divider

function RowHeading({ lable }) {
  return (
    <Grid xs={12} item>
      <Box sx={{
        padding: "10px 30px", border: '1px solid #8A2BE2', width: "30%", margin: "10px auto",
      }}
      >
        <Typography style={{ color: "#8A2BE2", textAlign: "center" }}>
          {lable}
        </Typography>
      </Box>
    </Grid>
  );
}

export { Tinput, RowHeading, DropdownInput };
