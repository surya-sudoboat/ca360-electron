/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  TextField, Grid, Button, Typography, Box, FormControl, Select, MenuItem, List,
  ListItem, Link, Divider,
} from '@material-ui/core';
import CircularProgress from '@mui/material/CircularProgress';
import Tooltip from '@mui/material/Tooltip';
import CloseIcon from '@mui/icons-material/Close';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { reload, resetTaskInfo } from '../../../actions/index';
import {
  getData, postData, putData, deleteData,
} from '../../Network/Index';
import { getCookie } from '../../cookie/Index';

// Styling of Dropdown menu
const MenuProps = {
  getContentAnchorEl: null,
  PaperProps: {
    style: {
      maxHeight: 200,
      width: 220,
      marginTop: 60,
    },
  },
};

function TaskForm({ mode, handleClose }) {
  // This is for getting data from database
  const initialValues = useSelector((state) => state.NewTaskData);
  const taskId = useSelector((state) => state.selectedTaskId);
  const [refresh, setRefresh] = useState(false);
  // Variable for storing form data.
  const [values, setValues] = useState(initialValues);
  const [file, setFile] = useState('');
  const [isLoading, setIsLoading] = useState(true);
  // Variable to use dispatch
  const dispatch = useDispatch();
  // Variable to strore user List
  const [users, setUsers] = useState([]);
  // Variable to store user name to assign task
  const [option, setOption] = useState([]);
  // variable to check login user role
  const isAdmin = getCookie('isAdmin') === 'true';
  // Variable for styling of button
  const btnStyle = { margin: 20, padding: 10 };

  // function to handle change in form.
  const handleInputChange = (e) => {
    if (e.target.name === 'uploadFile') {
      setFile(e.target.files);
    } else {
      const { name, value } = e.target;
      setValues({ ...values, [name]: value });
    }
  };

  // function to upadte list of users and Option variable
  useEffect(() => {
    // get request to fetch all users
    const url = '/user';
    getData(url)
      .then((res) => {
        setUsers(res.data.data);
        // Updating Option variable
        const temp = [];
        res.data.data.map((item) => {
          temp.push(item.name);
          return null;
        });
        setOption(temp);
      })
      .catch((error) => {
        toast.error(error.message);
      });
  }, []);

  useEffect(() => {
    if (mode === 'edit') {
      const taskUrl = `/task/${taskId}`;
      getData(taskUrl)
        .then((res) => {
          const data = {
            taskName: res.data.data.taskName,
            assignedTo: users.find((it) => it._id === res.data.data.assignedTo) !== undefined
              ? users.find((it) => it._id === res.data.data.assignedTo).name
              : '',
            description: res.data.data.description,
            uploadFile: res.data.data.uploadFile,
          };
          setValues(data);
          setIsLoading(false);
        })
        .catch((error) => {
          toast.error(error.response.data.message);
        });
    } else { setIsLoading(false); }
  }, [refresh, users]);

  // function to upload file IN EDIT MODE
  const UploadFile = () => {
    if (file.length === 0) {
      toast.error('Please select file to upload');
    } else {
      const data = new FormData();
      for (let i = 0; i < file.length; i += 1) {
        data.append('uploadFile', file[i]);
      }
      const url = `/task/${taskId}/upload`;
      // put request to upload file
      putData(data, url)
        .then((res) => {
          toast.success(res.data.message);
          setRefresh(!refresh);
        })
        .catch((error) => {
          toast.error(error.response.data.message);
        });
    }
  };

  // function to handle submit of form
  const handleSubmit = (e) => {
    e.preventDefault();
    // finding user id by name
    const userId = users.filter((item) => item.name === values.assignedTo);
    // Checking if mode is create new task or edit task
    if (mode === 'create') {
      const data = new FormData();
      data.append('taskName', values.taskName);
      data.append('description', values.description);
      data.append('assignedTo', userId[0]._id);
      for (let i = 0; i < file.length; i += 1) {
        data.append('uploadFile', file[i]);
      }
      const url = '/task';
      // post request to create new task
      postData(data, url)
        .then((res) => {
          toast.success(res.data.message);
          dispatch(reload());
          handleClose();
        })
        .catch((error) => {
          toast.error(error.response.data.message);
        });
    } else {
      // put request to update task
      const data = { taskName: values.taskName, description: values.description, assignedTo: userId[0]._id };
      const url = `/task/${taskId}`;
      UploadFile();
      putData(data, url)
        .then((res) => {
          toast.success(res.data.message);
          dispatch(resetTaskInfo());
          dispatch(reload());
          handleClose();
        })
        .catch((error) => {
          toast.error(error.response.data.message);
        });
    }
  };
  // function to handle delete of task
  const handleDelete = () => {
    const url = `/task/${taskId}`;
    // delete request to delete task
    deleteData(url)
      .then((res) => {
        toast.error(res.data.message);
        dispatch(reload());
        handleClose();
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  };
  // function to delete file
  const DeleteFile = (path) => {
    const url = `/task/${taskId}/remove`;
    const data = { files: [path] };
    // delete request to delete file
    deleteData(url, data)
      .then((res) => {
        toast.error(res.data.message);
        setRefresh(!refresh);
      })
      .catch((error) => {
        toast.error(error.response.data.message);
        setRefresh(!refresh);
      });
  };

  return (
    <Box>
      {/* Wait untill data is get through API */}
      {!isLoading ? (
        // Instyling used to override default styling of Material UI
        <FormControl sx={{ m: 1, minWidth: 150 }}>
          <form onSubmit={handleSubmit}>
            <Grid container spacing={2}>
              { mode === 'create' ? (
                <Grid item xs={12}>
                  <Typography variant="h6">
                    Add Task

                  </Typography>
                  <Divider />
                </Grid>
              ) : null}
              <Grid item xs={12}>
                <Typography variant="h6">
                  Task Name
                  <span className="redColor">*</span>
                </Typography>
                <TextField
                  variant="outlined"
                  multiline
                  maxRows={4}
                  value={values.taskName}
                  name="taskName"
                  fullWidth
                  onChange={handleInputChange}
                  required
                />
              </Grid>
              <Grid item xs={12}>
                <Typography variant="h6"> Description </Typography>
                <TextField
                  multiline
                  fullWidth
                  rows={4}
                  variant="outlined"
                  value={values.description}
                  onChange={handleInputChange}
                  name="description"
                />
              </Grid>
              <Grid item xs={12}>
                <Typography variant="h6">
                  Assigned To
                  <span className="redColor">*</span>
                </Typography>
                <FormControl fullWidth>
                  <Select
                    variant="outlined"
                    value={values.assignedTo}
                    onChange={handleInputChange}
                    name="assignedTo"
                    MenuProps={MenuProps}
                    required
                    fullWidth
                  >
                    {option.map((item, index = 0) => (
                      <MenuItem value={item} key={index}>
                        {item}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12}>
                <Typography variant="h6"> Upload File </Typography>
                <input type="file" accept=".png, .jpg, .jpeg, .pdf, .PNG, .JPEG, .JPG, .docx, .doc, .DOCX, .DOC" style={{ marginTop: 10 }} name="uploadFile" onChange={handleInputChange} multiple />
                {mode === 'edit' && (
                  <List>
                    <ListItem>
                      {/* <Button variant="contained" color="primary" onClick={() => UploadFile()} style={{ marginLeft: "-15px" }}>
                        Upload File
                      </Button> */}
                    </ListItem>
                    {values.uploadFile.map((item, index = 0) => (
                      <ListItem key={index}>
                        <Link href={`https://enigmatic-chamber-52575.herokuapp.com${item}`} underline="none" target="_blank">
                          { item.split('/')[2] }
                        </Link>
                        <Tooltip title="Delete File">
                          <Button onClick={() => DeleteFile(item)} color="secondary" size="large">
                            <CloseIcon />
                          </Button>
                        </Tooltip>
                      </ListItem>
                    ))}
                  </List>
                )}
              </Grid>
              {mode === 'edit' ? (
                <Grid item xs={12}>
                  {isAdmin && (
                  <Button
                    onClick={() => { handleDelete(); }}
                    variant="text"
                    style={{
                      padding: "5px 15px", float: "right", margin: 5, border: "1px solid rgba(0, 0, 0, 0.5)",
                    }}
                  >
                    Delete
                  </Button>
                  )}
                  <Button
                    color="primary"
                    variant="contained"
                    type="submit"
                    style={{
                      padding: "5px 15px", float: "right", background: "#8A2BE2", margin: 5,
                    }}
                  >
                    Save
                  </Button>
                </Grid>
              ) : (
                <Grid item xs={12}>
                  <Button
                    onClick={() => { handleClose(); }}
                    variant="text"
                    style={{
                      padding: "5px 15px", float: "right", margin: 5, border: "1px solid rgba(0, 0, 0, 0.5)",
                    }}
                  >
                    Cancel
                  </Button>
                  <Button
                    color="primary"
                    variant="contained"
                    type="submit"
                    style={{
                      padding: "5px 15px", float: "right", background: "#8A2BE2", margin: 5,
                    }}
                  >
                    Create
                  </Button>
                </Grid>
              )}
            </Grid>
          </form>
        </FormControl>
      ) : (
        <Box style={{ height: "100px", width: "100px" }}>
          <CircularProgress size={50} style={{ color: "#8A2BE2", margin: "10px" }} />
        </Box>
      )}
      <ToastContainer />
    </Box>
  );
}

export default TaskForm;
