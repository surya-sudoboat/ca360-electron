/* eslint-disable react/jsx-no-bind */
/* eslint-disable react/jsx-props-no-multi-spaces */
import React, { useState } from "react";
import {
  Grid, TextField, Typography, Button, Paper, IconButton, OutlinedInput, FormControl,
} from '@material-ui/core';
import { InputAdornment } from "@mui/material";
import { toast } from 'react-toastify';
import { useDispatch, useSelector } from "react-redux";
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Visibility from '@mui/icons-material/Visibility';
import {
  reload, userPageState, userIdChange, // resetUserInfo,
} from "../../../actions/index";
import { postData, putData } from "../../Network/Index";

export default function NewUserForm() {
  // variable for dispatch an action
  const dispatch = useDispatch();
  const userId = useSelector((state) => state.userIdChange);
  const userData = useSelector((state) => state.userData);
  const currentState = useSelector((state) => state.userPageState);
  // variables to save the input of each text fields.
  const [values, setValues] = useState(userData);
  // const [name, setName] = useState("");
  // const [username, setUsername] = useState("");
  // const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  // variables to check if an input is not in a required manner.
  // const [nameError, setNameError] = useState(false);
  const [usernameError, setUsernameError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);
  // eslint-disable-next-line max-len
  // function to save the input of text fields to their respective state variables and to check whether an input is in its required manner.
  const validateTextFields = (text, fieldType) => {
    setValues({ ...values, [fieldType]: text });
    if (fieldType === "name") {
    //  setName(text);
    /*  if (!text.match(/^[a-zA-Z]+ [a-zA-Z]+$/)) {
        setNameError(true);
      } else {
        setNameError(false);
      }
    */ }
    if (fieldType === "username") {
    //  setUsername(text);
      if (
        !text.match(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        )
      ) {
        setUsernameError(true);
      } else {
        setUsernameError(false);
      }
    }

    if (fieldType === "password") {
    //  setPassword(text);
      if (text.length < 3) {
        setPasswordError(true);
      } else {
        setPasswordError(false);
      }
    }
  };
  // function to make the password visible or not visible depending on the icon clicked.
  function passwordVisibility() {
    setShowPassword(!showPassword);
  }
  // eslint-disable-next-line max-len
  // function called to pop up error if text field(s) are not in the required manner else post data of text fields through post API.
  const handleSubmit = (e) => {
    e.preventDefault();
    if (currentState === "create") {
      if (values.name === "") { // || nameError
        toast.error("Check Name");
      } else if (values.username === "" || usernameError) {
        toast.error("Check Username");
      } else if (values.password === "" || passwordError) {
        toast.error("Check Password");
      } else {
        const url = "/user/";
        // const data = { username, password, name };
        postData(values, url).then((res) => { // data
          dispatch(userIdChange(res.data.data._id));
          dispatch(userPageState("userView"));
          dispatch(reload());
          toast.success("User created successfully.");

          // eslint-disable-next-line max-len
          // dispatch reload function will called to update the users list with the new user who is created just now.
          dispatch(reload());
        })
          .catch((error) => {
            toast.error(error.response.data.message);
          });
      }
    } else {
      const url = `/user/${userId}`;
      putData(values, url).then((res) => {
        toast.success(res.data.message);
        dispatch(reload());
        // dispatch(resetUserInfo());
      }).catch((error) => {
        toast.error(error.response.data.message);
      });
      dispatch(userPageState("userView"));
    }
  };
  return (
    <Grid>
      <Paper elevation={0} className="formPaper">
        <form onSubmit={handleSubmit}>
          <Grid container justifyContent="space-around" spacing={2} alignItems="center">
            <Grid item xs={5}>
              <Typography style={{
                fontFamily: 'Helvetica',
                fontStyle: "normal",
                fontWeight: "400",
                fontSize: "13px",
              }}
              >
                Name

              </Typography>
              <TextField
                size="small"
                variant="outlined"
                value={values.name}
                onChange={(e) => {
                  validateTextFields(e.target.value, "name");
                }}
              />
            </Grid>
            <Grid item xs={5}>
              <Typography style={{
                fontFamily: 'Helvetica',
                fontStyle: "normal",
                fontWeight: "400",
                fontSize: "13px",
              }}
              >
                UserName

              </Typography>
              <TextField
                size="small"
                variant="outlined"
                value={values.username}
                onChange={(e) => {
                  validateTextFields(e.target.value, "username");
                }}
              />
            </Grid>
            {(currentState === "create") ? (
              <>
                <Grid item xs={5}>
                  <Typography>password</Typography>
                  <FormControl fullWidth size="small">
                    <OutlinedInput
                      style={{
                        fontFamily: 'Helvetica',
                        fontStyle: "normal",
                        fontWeight: "400",
                        fontSize: "13px",
                      }}
                      id="passwordField"
                      size="small"
                      placeholder="min 3 chars"
                      type={showPassword ? "text" : "password"}

                      endAdornment={(
                        <InputAdornment position="end">

                          <IconButton onClick={passwordVisibility}>
                            {showPassword ? <Visibility />
                              : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                  )}

                      fullWidth
                      variant="outlined"
                      onChange={(e) => {
                        validateTextFields(e.target.value, "password");
                      }}
                    />
                  </FormControl>

                </Grid>
                <Grid item xs={5} />

              </>
            )
              : null }
            <Grid item xs={5}>
              <Button style={{ background: " #8A2BE2", color: "#FFFFFE" }} fullWidth type="submit" variant="contained">Submit</Button>
            </Grid>
          </Grid>
        </form>
      </Paper>
    </Grid>
  );
}
