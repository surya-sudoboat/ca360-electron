/* eslint-disable react/jsx-no-useless-fragment */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import { toast } from 'react-toastify';
import {
  Grid, TextField, Typography, Button, Paper,
} from '@material-ui/core';
import CircularProgress from '@mui/material/CircularProgress';
import { useSelector, useDispatch } from "react-redux";
import { reload, userPageState } from "../../../actions/index";
import { getData, putData } from "../../Network/Index";

export default function EditUserForm() {
  // variable for dispatch an action
  const dispatch = useDispatch();
  // variable to get data from store
  const userId = useSelector((state) => state.userIdChange);
  const userData = useSelector((state) => state.userData);

  // data variable is to get details of a particular user from API.
  const [data, setData] = React.useState({});
  // variables to check if an input is not in a required manner.
  // const [nameError, setNameError] = useState(false);
  const [usernameError, setUsernameError] = useState(false);
  // eslint-disable-next-line max-len
  // variable loading would be false only if data of a particular role is fetched from API and saved to data variable.
  const [loading, setLoading] = React.useState(true);
  // Styling for loader
  const loaderStyle = { color: "white" };

  // useEffect to get data from API
  useEffect(() => {
    const url = `/user/${userId}`;
    getData(url).then((res) => {
      setData(res.data.data);
      setLoading(false);
    });
  }, []);

  // function called to update the data variable.
  const validateTextFields = (text, fieldType) => {
    if (fieldType === "name") {
      setData({ ...data, name: text });
      /* if (!text.match(/^[a-zA-Z]+ [a-zA-Z]+$/)) {
        setNameError(true);
      } else {
        setNameError(false);
      }
    */ }
    if (fieldType === "username") {
      setData({ ...data, username: text });
      if (
        !text.match(
          /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        )
      ) {
        setUsernameError(true);
      } else {
        setUsernameError(false);
      }
    }
  };
  // eslint-disable-next-line max-len
  // function called to pop up error if data is not in the required manner else update data in through put API.
  const handleEdit = (e) => {
    e.preventDefault();
    const url = `/user/${userId}`;
    const body = { username: data.username, name: data.name };
    if (data.name === "") { // || nameError
      toast.error("Check Name");
    } else if (data.username === "" || usernameError) {
      toast.error("Check Username");
    } else {
      putData(body, url).then((res) => {
        console.log(res);
        toast.success(res.data.message);
        dispatch(reload());
      }).catch((error) => {
        toast.error(error.response.data.message);
      });
      dispatch(userPageState("userView"));
    }
  };
  return (
    <>
      { (loading) ? <div className="rightLoader"><CircularProgress size={50} style={loaderStyle} /></div>
        : (
          <Grid>
            <Paper elevation={0} className="formPaper">
              <form onSubmit={handleEdit}>

                <Grid container justifyContent="space-around" spacing={2}>
                  <Grid item xs={5}>
                    <Typography style={{
                      fontFamily: 'Helvetica',
                      fontStyle: "normal",
                      fontWeight: "400",
                      fontSize: "13px",
                    }}
                    >
                      Name

                    </Typography>
                    <TextField
                      size="small"
                      value={data.name}
                      variant="outlined"
                      onChange={(e) => {
                        validateTextFields(e.target.value, "name");
                      }}
                    />
                  </Grid>
                  <Grid item xs={5}>
                    <Typography style={{
                      fontFamily: 'Helvetica',
                      fontStyle: "normal",
                      fontWeight: "400",
                      fontSize: "13px",
                    }}
                    >
                      UserName

                    </Typography>

                    <TextField
                      size="small"
                      value={data.username}
                      variant="outlined"
                      onChange={(e) => {
                        validateTextFields(e.target.value, "username");
                      }}
                    />
                  </Grid>

                  <Grid item xs={5}>
                    <Button style={{ background: " #8A2BE2", color: "#FFFFFE" }} type="submit" fullWidth variant="contained">Submit</Button>
                  </Grid>
                </Grid>
              </form>
            </Paper>
          </Grid>
        )}
    </>
  );
}
