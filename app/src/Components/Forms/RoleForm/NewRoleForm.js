import React, { useState } from "react";
import {
  Grid, TextField, Typography, Button, Paper,
} from '@material-ui/core';
import { toast } from 'react-toastify';
import { useDispatch } from "react-redux";
import { reload, rolePageState, roleIdChange } from "../../../actions/index";
import { postData } from "../../Network/Index";

export default function NewRoleForm() {
  // variable for dispatch an action
  const dispatch = useDispatch();
  const [role, setRole] = useState("");
  // function to save the input text in the role variable.
  const validateTextFields = (text, fieldType) => {
    if (fieldType === "role") {
      setRole(text);
    }
  };
  // eslint-disable-next-line max-len
  // function called to pop up error if role text field is empty string else post the value of role text field through post API.
  const handleSubmit = (e) => {
    e.preventDefault();

    const url = "/role/";
    const data = { role };
    if (data.role === "") {
      toast.error("Invalid Input");
    } else {
      postData(data, url).then((res) => {
        if (res.data.status === "Employee's are not allowed.") {
          toast.error(res.data.status);
        } else {
          dispatch(roleIdChange(res.data.data._id));
          dispatch(rolePageState("roleView"));
        }

        dispatch(reload());
      })
        .catch((res) => { toast.error(res.data); });
      dispatch(rolePageState("roleView"));
      dispatch(roleIdChange(null));
      dispatch(reload());
    }
  };

  return (
    <Grid>
      <Paper elevation={0} className="formPaper">
        <form onSubmit={handleSubmit}>
          <Grid container justifyContent="space-around" spacing={2} alignItems="center">

            <Grid item xs={5}>
              <Typography style={{
                fontFamily: 'Helvetica',
                fontStyle: "normal",
                fontWeight: "400",
                fontSize: "13px",
              }}
              >
                Role

              </Typography>
              <TextField
                size="small"
                variant="outlined"
                onChange={(e) => {
                  validateTextFields(e.target.value, "role");
                }}
              />
            </Grid>

            <Grid item xs={5} />
            <Grid item xs={5}>
              <Button style={{ background: " #8A2BE2", color: "#FFFFFE" }} fullWidth type="submit" variant="contained">Submit</Button>
            </Grid>
          </Grid>
        </form>
      </Paper>
    </Grid>
  );
}
