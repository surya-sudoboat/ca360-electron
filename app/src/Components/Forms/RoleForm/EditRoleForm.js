/* eslint-disable react/jsx-no-useless-fragment */
import React, { useEffect } from "react";
import { toast } from 'react-toastify';
import {
  Grid, TextField, Typography, Button, Paper,
} from '@material-ui/core';
import CircularProgress from '@mui/material/CircularProgress';
import { useSelector, useDispatch } from "react-redux";
import { reload, rolePageState } from "../../../actions/index";
import { getData, putData } from "../../Network/Index";

export default function EditUserForm() {
  // variable for dispatch an action
  const dispatch = useDispatch();
  const roleId = useSelector((state) => state.roleIdChange);
  // data variable is to get details of a particular role from API.
  const [data, setData] = React.useState({});
  // eslint-disable-next-line max-len
  // variable loading would be false only if data of a particular role is fetched from API and saved to data variable.
  const [loading, setLoading] = React.useState(true);
  // Styling for loader
  const loaderStyle = { color: "white" };

  // useEffect to get data from API
  useEffect(() => {
    const url = `/role/${roleId}`;
    getData(url).then((res) => {
      setData(res.data.data);
      setLoading(false);
    });
  }, []);
  // function called to update the data variable.
  const validateTextFields = (text, fieldType) => {
    if (fieldType === "role") {
      setData({ ...data, role: text });
    }
  };
  // eslint-disable-next-line max-len
  // function called to pop up error if role text field is empty else update data in the backend through put API.
  const handleEdit = () => {
    const url = `/role/${roleId}`;
    const body = { username: data.username, name: data.name };
    if (data.role === "") {
      toast.error("Invalid Input");
    } else {
      putData(body, url).then((res) => {
        console.log(res);
        if (res.data.message === "You are not allowed." || res.data.message === "Username already exists.") {
          toast.error(res.data.message);
        } else {
          dispatch(reload());
          console.log(res.data.message);
          toast.success(res.data.message);
        }
      }).catch((error) => {
        toast.error(error.response.data.message);
      });
    }
    dispatch(rolePageState("roleView"));
  };
  return (
    <>
      { (loading) ? <div className="rightLoader"><CircularProgress size={50} style={loaderStyle} /></div>
        : (
          <Grid>
            <Paper elevation={0} className="formPaper">
              <form onSubmit={handleEdit}>

                <Grid container justifyContent="space-around" spacing={2}>
                  <Grid item xs={5}>
                    <Typography style={{
                      fontFamily: 'Helvetica',
                      fontStyle: "normal",
                      fontWeight: "400",
                      fontSize: "13px",
                    }}
                    >
                      Role

                    </Typography>

                    <TextField
                      size="small"
                      value={data.role}
                      variant="outlined"
                      onChange={(e) => {
                        validateTextFields(e.target.value, "role");
                      }}
                    />
                  </Grid>
                  <Grid item xs={5} />
                  <Grid item xs={5}>
                    <Button style={{ background: " #8A2BE2", color: "#FFFFFE" }} type="submit" fullWidth variant="contained">Submit</Button>
                  </Grid>
                </Grid>
              </form>
            </Paper>
          </Grid>
        )}
    </>
  );
}
