import React from "react";
import { Button, Typography } from "@material-ui/core";
import CloseIcon from "@mui/icons-material/Close";

function Popup({ handleClose, handleDelete, handleCancel }) {
  return (
    <div className="popup-box">
      <div className="box">
        <span className="close-icon">
          <CloseIcon onClick={handleClose} />
        </span>
        <Typography
          style={{
            fontFamily: "Helvetica",
            fontStyle: "normal",
            fontWeight: "400",
            fontSize: "18px",
            color: "#000000",
            marginTop: "8%",
          }}
        >
          Are you sure to delete ?
        </Typography>
        <Typography
          style={{
            fontFamily: "Helvetica",
            fontStyle: "normal",
            fontWeight: "400",
            fontSize: "13px",
          }}
        >
          If you delete this you can’t recover it.
        </Typography>
        <div style={{ display: "flex", marginLeft: "57%", marginTop: "17%" }}>
          <Button
            style={{
              width: "35%",
              height: "12%",
              background: "#F4F5F0",
              color: "#000000",
              border: "1px solid rgba(0, 0, 0, 0.5)",
            }}
            onClick={handleCancel}
          >
            <Typography
              style={{
                fontFamily: "Helvetica",
                fontStyle: "normal",
                fontWeight: "400",
                fontSize: "12px",
                textTransform: "none",
              }}
            >
              Cancel
            </Typography>
          </Button>
          <Button
            style={{
              width: "35%",
              height: "12%",
              background: "#8A2BE2",
              color: "#F4F5F0",
              marginLeft: "6%",
              border: "1px",
            }}
            onClick={handleDelete}
          >
            <Typography
              style={{
                fontFamily: "Helvetica",
                fontStyle: "normal",
                fontWeight: "400",
                fontSize: "12px",
                textTransform: "none",
              }}
            >
              Delete
            </Typography>
          </Button>
        </div>
      </div>
    </div>
  );
}

export default Popup;
