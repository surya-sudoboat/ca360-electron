import React from "react";
import { useDispatch } from "react-redux";

import { Typography, TextField, Box } from '@material-ui/core';
import InputAdornment from '@mui/material/InputAdornment';
// import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
// import AccountCircleRoundedIcon from '@mui/icons-material/AccountCircleRounded';
import PersonIcon from '@mui/icons-material/Person';
import SearchIcon from '@mui/icons-material/Search';
import {
  searchValue,
  search,
} from "../../actions/index";

export default function HeaderNew({ heading }) {
  // variable for dispatch
  const dispatch = useDispatch();
  return (
    <Box style={{
      display: "flex", justifyContent: "space-between", margin: "20px 0px 20px 0px", alignSelf: "center", marginRight: "1%",
    }}
    >
      <Box style={{ width: "60%" }}>
        <Typography style={{ fontSize: "25px", fontWeight: "700" }}>{heading}</Typography>
      </Box>
      <Box style={{
        width: "29%", display: "flex", justifyContent: "space-between", alignItems: "center", //  width: "25%"
      }}
      >
        <TextField
          style={{
            display: "flex", alignItems: "center", background: "rgba(238, 219, 255, 0.99)", width: "220px", height: "26px", borderRadius: "4px", padding: "0px", paddingTop: "0px",
          }}
          variant="standard"
          size="small"
          placeholder="Search..."
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon style={{
                  margin: "2px 5px 2px 15px", padding: "0px", paddingTop: "0px", color: "black",
                }}
                />
              </InputAdornment>
            ),
            disableUnderline: true,
          }}
          onChange={(e) => {
            dispatch(search(true));
            dispatch(searchValue(e.target.value));
          }}
        />
        {/* <NotificationsNoneIcon style={{ color: "#8A2BE2" }} fontSize="large" /> */}
        {/* <AccountCircleRoundedIcon
        style={{ color: "#8A2BE2", borderRadius: "100%", alignSelf: "center" }}
         fontSize="large" /> */}
        <div style={{ background: "#EEDBFFFC", borderRadius: "20px" }}>
          <PersonIcon style={{ color: "#8A2BE2" }} />
        </div>

      </Box>
    </Box>
  );
}
