import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  AppBar, Toolbar, Typography, Box, Tooltip, IconButton, Menu, MenuItem,
} from '@material-ui/core';
import { ToastContainer, toast } from 'react-toastify';
import AccountCircleRoundedIcon from '@mui/icons-material/AccountCircleRounded';
import { ResetStore } from '../../actions/index';
import { deleteCookie, getCookie } from "../cookie/Index";
import { getData } from '../Network/Index';

function Header() {
  // Variable and function for Dropdown menu of Icon
  const [anchorElUser, setAnchorElUser] = useState(null);
  const [UserName, setUserName] = useState('');
  const reload = useSelector((state) => state.reloadPage);
  // Varibales for styling
  const headerStyle = { background: '#2c4e86' };
  // Variable for dispatch
  const dispatch = useDispatch();

  // Function to get UserName from cookie
  useEffect(() => {
    const userId = getCookie('UserId');
    if (userId) {
      getData(`/user/${userId}`)
        .then((res) => {
          if (res.status === 200) {
            setUserName(res.data.data.name);
          }
        })
        .catch((error) => {
          toast.error(error.response.data.message);
        });
    }
  }, [reload]);

  // Function To Dropdown menu of Icon
  const handleOpenUserMenu = (event) => {
    setAnchorElUser(event.currentTarget);
  };

  // Function To Close Dropdown menu of Icon
  const handleCloseUserMenu = () => {
    setAnchorElUser(null);
  };

  // Function To handle Logout
  const handleLogout = () => {
    // Reset the store
    dispatch(ResetStore());
    // Delete the cookies
    deleteCookie(`token=${`${getCookie("token")};max-age=0`}`);
    deleteCookie(`UserId=${`${getCookie("UserId")};max-age=0`}`);
    deleteCookie(`isAdmin=${`${getCookie("isAdmin")};max-age=0`}`);
  };

  return (
    <div>
      <AppBar style={headerStyle}>
        <Toolbar>
          {/* Code for logo */}
          <Typography variant="h6" className="handleFlow">
            CA360
          </Typography>
          {/* Code for user name */}
          <Typography variant="h6" className="UserName">
            {UserName}
          </Typography>
          {/* Code for Icon and Its dropdown menu */}
          <Box>
            <Tooltip title="Open profile">
              <IconButton onClick={handleOpenUserMenu}>
                <AccountCircleRoundedIcon
                  className="headerIcon"
                  fontSize="large"
                />
              </IconButton>
            </Tooltip>
            <Menu
              anchorEl={anchorElUser}
              style={{ margin: '30px 10px' }}
              anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              keepMounted
              transformOrigin={{
                vertical: 'top',
                horizontal: 'right',
              }}
              open={Boolean(anchorElUser)}
              onClose={handleCloseUserMenu}
            >
              <MenuItem onClick={handleCloseUserMenu}>
                <Typography>
                  {/* Linking to Loginpage on click */}
                  <Link to="/" onClick={handleLogout} className="DropdownItem">
                    Logout
                  </Link>
                </Typography>
              </MenuItem>
            </Menu>
          </Box>
        </Toolbar>
      </AppBar>
      <ToastContainer />
    </div>
  );
}

export default Header;
