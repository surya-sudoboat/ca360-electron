/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import CircularProgress from '@mui/material/CircularProgress';
import {
  Typography, Paper, Box, Divider,
} from '@material-ui/core';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { getData, putData } from '../../Network/Index';
import Task from './Task';
import { getCookie } from '../../cookie/Index';

function Tasks() {
  const isAdmin = getCookie('isAdmin') === 'true';
  const UserId = getCookie('UserId');
  const search = useSelector((state) => state.search);
  const searchItem = useSelector((state) => state.searchValue);
  // Variable to strore user list
  const [users, setUsers] = useState([]);
  // Styling for loader
  const loaderStyle = { color: "#8A2BE2" };
  // Styling of Dropdown menu
  // const MenuProps = {
  //   getContentAnchorEl: null,
  //   PaperProps: {
  //     style: {
  //       maxHeight: 20,
  //       width: 60,
  //       marginTop: 10,
  //     },
  //   },
  // };
  // function to handle change in form.

  // This is for Drag and drop functionality
  const onDragEnd = (result, columns, setColumns) => {
    if (!result.destination) return;
    const { source, destination } = result;

    if (source.droppableId !== destination.droppableId) {
      const sourceColumn = columns[source.droppableId];
      const destColumn = columns[destination.droppableId];
      const sourceItems = [...sourceColumn.items];
      const destItems = [...destColumn.items];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...sourceColumn,
          items: sourceItems,
        },
        [destination.droppableId]: {
          ...destColumn,
          items: destItems,
        },
      });
      // Giving put request to update record in database
      const status = { status: `${result.destination.droppableId}` };
      const url = `/task/${result.draggableId}`;
      putData(status, url)
        .then((res) => {
          toast.success(res.data.message);
        })
        .catch((error) => {
          toast.error(error.response.data.message);
        });
    } else {
      const column = columns[source.droppableId];
      const copiedItems = [...column.items];
      const [removed] = copiedItems.splice(source.index, 1);
      copiedItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...column,
          items: copiedItems,
        },
      });
    }
  };

  // This is for getting data from database
  const reload = useSelector((state) => state.reloadPage);
  // Variables
  const [isLoading, setIsLoading] = useState(true);
  const [data, setdata] = useState([]);
  // Initializing variables Of Coloums with empty Items
  const [columns, setColumns] = useState({
    open: {
      name: 'Open',
      items: [],
    },
    inProgress: {
      name: 'In Progress',
      items: [],
    },
    closed: {
      name: 'Closed',
      items: [],
    },
  });

  useEffect(() => {
    const url = '/user';
    getData(url)
      .then((res) => {
        setUsers(res.data.data);
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  }, []);

  // Making get request to get All task list

  useEffect(() => {
    const url = '/task';
    getData(url)
      .then((res) => {
        if (!isAdmin) {
          // userTask will save task(s) of particular user only not the task(s) of all users.
          console.log(!isAdmin);
          const userTask = res.data.data.filter((item) => item.assignedTo === UserId);

          setdata(userTask);
          if (search) {
            setdata(userTask.filter((item) => item.taskName?.includes(searchItem)));
          } else {
            setdata(userTask);
          }
          setIsLoading(false);
        } else {
        // eslint-disable-next-line no-plusplus
          for (let i = 0; i < res.data.data.length; i++) {
          // eslint-disable-next-line max-len
            res.data.data[i].assignedUser = users.find((it) => it._id === res.data.data[i].assignedTo)?.name;
          }

          if (search) {
            if (searchItem === "All") {
              setdata(res.data.data);
            } else {
              setdata(res.data.data.filter((item) => item.taskName?.includes(searchItem)
           || item.assignedUser?.includes(searchItem)));
            }
          } else {
            setdata(res.data.data);
          }
          setIsLoading(false);
        }
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  }, [reload, searchItem, users]);

  // Giving Intial Value of task List by checking the status of task

  useEffect(() => {
    // Filtiring the data by status
    const openL = data.filter((item) => item.status === 'open');
    const progressL = data.filter((item) => item.status === 'inProgress');
    const closedL = data.filter((item) => item.status === 'closed');
    const prev = {
      open: {
        name: 'Open',
        items: openL,
      },
      inProgress: {
        name: 'In Progress',
        items: progressL,
      },
      closed: {
        name: 'Closed',
        items: closedL,
      },
    };
    // Setting the value of columns
    setColumns(prev);
    // setIsLoading(false);
  }, [data]);

  // Code to manage drag and drop functionality
  return (
    <Paper
      elevation={0}
      style={{
        display: "flex", height: "80%", margin: "10px", justifyContent: "center", // padding: "12px 20% 2px 20%",
      }}
    >

      {/* Wait Untill data is updated to Coloum */}
      {console.log(search)}
      {!isLoading ? (
        <div style={{ width: "95%" }}>
          {(data.length === 0) ? (<Typography variant="h4" style={{ textAlign: "center" }}>No record found!</Typography>)
            : (
              <div style={{
                display: "flex", marginTop: "2%", justifyContent: "space-between", width: "100%",
              }}
              >
                <DragDropContext
                  onDragEnd={(result) => onDragEnd(result, columns, setColumns)}
                >

                  {Object.entries(columns).map(([columnId, column]) => (
                    <div className="Coloums" style={{ background: "#C4C4C433", height: "fit-content", width: "30%" }} key={columnId}>
                      <Divider
                        style={
                        // eslint-disable-next-line no-nested-ternary
                        column.name === "Open"
                          ? ({ background: "#979797", width: "100%", height: "4px" })
                          : (column.name === "In Progress" ? ({ background: "#F39D4D", width: "100%", height: "4px" })
                            : ({ background: "#038800", width: "100%", height: "4px" }))
                      }
                      />
                      <div style={{
                        display: "flex", width: "94%", justifyContent: "space-between", alignItems: "center", height: "50px",
                      }}
                      >
                        <Typography style={{

                          color: "#161616",
                          textAlign: "left",
                          fontFamily: "Helvetica",
                          fontStyle: "regular",
                          fontWeight: "400",
                          fontSize: "22px",
                        }}
                        >
                          {
                        // eslint-disable-next-line no-nested-ternary
                        column.name === "Open"
                          ? ("PLANNED")
                          : (column.name === "In Progress" ? ("IN PROGRESS")
                            : ("COMPLETED"))
                      }

                        </Typography>
                        <Box style={{
                          height: "30px", width: "30px",
                        }}
                        >
                          <Typography style={{ background: "white", color: "red", textAlign: "center" }}>
                            {' '}
                            {column.items.length}
                          </Typography>

                        </Box>
                      </div>
                      <div style={{ width: "94%" }}>
                        <Droppable droppableId={columnId} key={columnId}>
                          {(provided) => (
                            <div
                              {...provided.droppableProps}
                              ref={provided.innerRef}
                          // Instyling used to override the default style of draggable
                              style={{
                                paddingBottom: 0.5,
                                width: "100%",
                                maxHeight: 420, // 540
                                overflow: 'auto',
                              }}
                            >
                              <Task column={column} />

                              {provided.placeholder}
                            </div>
                          )}
                        </Droppable>
                      </div>
                    </div>
                  ))}
                </DragDropContext>
              </div>
            )}
        </div>
      ) : (
        <div>
          <CircularProgress size={50} style={loaderStyle} />
        </div>
      )}
      {/* Toast Container is used to give notification bealtifyly */}
      <ToastContainer />
    </Paper>
  );
}

export default Tasks;
