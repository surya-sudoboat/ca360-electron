/* eslint-disable import/no-named-as-default */
/* eslint-disable max-len */
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { Draggable } from 'react-beautiful-dnd';
import CircularProgress from '@mui/material/CircularProgress';
import PropTypes from 'prop-types';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { Typography } from '@material-ui/core';
import { toast, ToastContainer } from 'react-toastify';
import TaskForm from '../../Forms/TaskForm/Index';
import { setTaskId, resetTaskInfo } from '../../../actions/index';
import { getData } from '../../Network/Index';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  // Variables
  const { children, onClose, ...other } = props;
  // Variable for styling of Cross icon button
  const crossbuttonStyle = {
    position: 'absolute',
    right: 8,
    top: 8,
    color: 'blue',
  };

  return (
    // Styling for Dialog title
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton aria-label="close" onClick={onClose} sx={crossbuttonStyle}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

BootstrapDialogTitle.propTypes = {
  onClose: PropTypes.func.isRequired,
};

function Task({ column }) {
  // Variables
  const dispatch = useDispatch();
  // Variable to strore user list
  const [users, setUsers] = useState([]);
  const [open, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  // Styling for loader
  const loaderStyle = { color: "#8A2BE2" };

  // Making get request to get user list

  useEffect(() => {
    const url = '/user';
    getData(url)
      .then((res) => {
        setUsers(res.data.data);
        setIsLoading(false);
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  }, []);

  // Function to open dialog and upadte redux state
  const handleClickOpen = (id) => {
    dispatch(setTaskId(id));
    setOpen(true);
  };

  // Function to close dialog and reset redux state
  const handleClose = () => {
    dispatch(resetTaskInfo());
    setOpen(false);
  };

  return (
    <div>
      {/* It wait to show Loader untill it get response through API */}
      {!isLoading
        // Map all the task list in Colloums
        ? (column.items.map((item, index) => (
          <Draggable key={item._id} draggableId={item._id} index={index}>
            {(provided, snapshot) => (
              <div>
                <div
                  onClick={() => {
                    handleClickOpen(item._id);
                  }}
                  onKeyUp={() => {
                    handleClickOpen(item._id);
                  }}
                  role="button"
                  tabIndex="0"
                  ref={provided.innerRef}
                  {...provided.draggableProps}
                  {...provided.dragHandleProps}
                  // Instyling for task list to overide default style of material ui.
                  style={{
                    userSelect: 'none',
                    padding: 16,
                    margin: '0 0 10px 0',
                    minHeight: '50px',
                    backgroundColor: snapshot.isDragging
                      ? 'white'
                      : 'white',
                    color: '#6C6C6C',
                    ...provided.draggableProps.style,
                  }}
                >
                  {item.taskName}
                  <br />
                  <Typography style={{ color: "black" }}>
                    Assigned To:
                    {' '}
                    {/* It checks for the assigned user Name */}
                    {users.find((it) => it._id === item.assignedTo) !== undefined
                      ? users.find((it) => it._id === item.assignedTo).name
                      : 'User Deleted'}
                  </Typography>
                </div>
                <BootstrapDialog onClose={handleClose} open={open}>
                  <BootstrapDialogTitle onClose={handleClose} />
                  <DialogActions>
                    {/* Onclicking Create task Form Popup Will Open in edit mode */}
                    <TaskForm mode="edit" handleClose={handleClose} />
                  </DialogActions>
                </BootstrapDialog>
                <ToastContainer />
              </div>
            )}
          </Draggable>
        ))) : (
          <div className="taskLoader">
            <CircularProgress size={50} style={loaderStyle} />
          </div>
        )}
    </div>
  );
}

export default Task;
