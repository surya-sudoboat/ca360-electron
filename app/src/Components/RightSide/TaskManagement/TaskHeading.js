/* eslint-disable no-unused-vars */
/* eslint-disable import/no-named-as-default */
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Button, Typography, Select, MenuItem,
} from '@material-ui/core';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { ToastContainer, toast } from 'react-toastify';
import {
  resetTaskInfo, searchValue, search,
} from '../../../actions/index';
import TaskForm from "../../Forms/TaskForm/Index";
import HeaderNew from "../../Header/HeaderNew";
import { getCookie } from '../../cookie/Index';
import 'react-toastify/dist/ReactToastify.css';
import { getData } from '../../Network/Index';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

function BootstrapDialogTitle(props) {
  // Variables
  const { children, onClose, ...other } = props;
  // Variable for styling of Cross icon button
  const crossbuttonStyle = {
    position: 'absolute',
    right: 8,
    top: 8,
    color: 'blue',
  };

  return (
    // Styling for Dialog title
    <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
      {children}
      {onClose ? (
        <IconButton aria-label="close" onClick={onClose} sx={crossbuttonStyle}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </DialogTitle>
  );
}

BootstrapDialogTitle.propTypes = {
  onClose: PropTypes.func.isRequired,
};

export default function TaskHeading() {
  const dispatch = useDispatch();
  const [value, setValue] = useState("All");
  const [option, setOption] = useState([]);
  const handleInputChange = (e) => {
    // elif (e.target.name === 'uploadFile') {
    //   setFile(e.target.files);
    // }
    const item = e.target.value;
    setValue(item);
    dispatch(search(true));
    dispatch(searchValue(item));
  };

  // Styling of Dropdown menu
  const MenuProps = {
    getContentAnchorEl: null,
    PaperProps: {
      style: {
        maxHeight: 200,
        width: 250,
        marginTop: 60,
      },
    },
  };
  // Variables
  // const dispatch = useDispatch();
  // variable to check login user role
  const isAdmin = getCookie('isAdmin') === 'true';
  const [open, setOpen] = React.useState(false);

  // Function to open Dialog
  const handleClickOpen = () => {
    setOpen(true);
  };
  // Function to close Dialog and reset task info
  const handleClose = () => {
    dispatch(resetTaskInfo());
    setOpen(false);
  };

  // Variable for create Style create new Button
  const button = {
    backgroundColor: '#8A2BE2',
    color: 'white',
    marginRight: "0.6%",
    padding: 10,
  };

  useEffect(() => {
    const url = '/user';
    getData(url)
      .then((res) => {
        // Updating Option variable
        const temp = [];
        // res.data.data.map((item) => {
        //   temp.push(item.name);
        //   return null;
        // });
        temp.push("All");
        // eslint-disable-next-line no-plusplus
        for (let i = 0; i < res.data.data.length; i++) {
          if (res.data.data[i].name !== "Admin") {
            temp.push(res.data.data[i].name);
          } else {
            // eslint-disable-next-line no-continue
            continue;
          }
        }

        setOption(temp);
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  }, []);

  return (
    <div style={{ height: "16%" }}>
      {' '}
      {/* 13% */}
      <HeaderNew heading="Task Management" />
      {isAdmin && (
      <div style={{
        display: "flex", justifyContent: "space-between", alignItems: "center",
      }}
      >

        <Select
          variant="outlined"
          style={{ height: "40px", width: "250px" }}
          value={value}
          MenuProps={MenuProps}
          onChange={handleInputChange}
          name="assignedTo"
          placeholder="Select User"
          required
        >
          {option.map((item, index = 0) => (
            <MenuItem value={item} key={index}>
              {item}
            </MenuItem>
          ))}
        </Select>

        {/* Create task is acces to admin Only. */}

        <Button style={button} onClick={handleClickOpen}>
          <Typography style={{
            fontFamily: "Helvetica",
            fontStyle: "normal",
            fontWeight: "700",
            fontSize: "13px",
            textTransform: "none",
          }}
          >
            Add Task
          </Typography>
        </Button>
      </div>
      )}

      <BootstrapDialog onClose={handleClose} open={open}>
        <BootstrapDialogTitle onClose={handleClose} />
        <DialogActions>
          {/* Onclicking Create task Form Popup Will Open in Create mode */}
          <TaskForm mode="create" handleClose={handleClose} />
        </DialogActions>
      </BootstrapDialog>
    </div>
  );
}
