import React from 'react';
import TaskHeading from './TaskHeading';
import AllTasks from './AllTasks';

function TasksPage() {
  return (
    <>
      {/* <div className="tasksContainer"> */}
      {/* <div className="taskSubheading">
        <TaskHeading />
      </div>
      <div className="taskContainer">
        <AllTasks />
      </div> */}
      <div className="rightSide" style={{ marginRight: "1%" }}>
        <div className="ClientSubheading">
          <TaskHeading />
        </div>

        <AllTasks />

      </div>
    </>
  );
}

export default TasksPage;
