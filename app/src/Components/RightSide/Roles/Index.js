import React from 'react';
import { useSelector } from "react-redux";
import RolesHeading from './RolesHeading';
import RolesList from './RolesList';
import RolesTabs from './RolesTabs';

function RolesPage() {
  const rolePageState = useSelector((state) => state.rolePageState);

  return (
    <div className="rightSide">
      <div className="ClientSubheading">
        <RolesHeading />
      </div>
      <div className="clientContainer">
        <RolesList />
        {(rolePageState === "view" || rolePageState === "create" || rolePageState === "edit")
          ? <RolesTabs /> : null}

      </div>
    </div>
  );
}

export default RolesPage;
