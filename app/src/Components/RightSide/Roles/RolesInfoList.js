/* eslint-disable react/jsx-no-useless-fragment */
/* eslint-disable react/style-prop-object */
import React, { useEffect } from "react";
import { Grid, Paper } from '@material-ui/core';
import CircularProgress from '@mui/material/CircularProgress';
import { useSelector } from "react-redux";
import { getData } from "../../Network/Index";

export default function RolesInfoList() {
  // variable to get data from store
  const roleId = useSelector((state) => state.roleIdChange);
  // data variable is to get details from Api of particular role.
  const [data, setData] = React.useState({});
  // eslint-disable-next-line max-len
  // variable loading would be false only if data of a particular role is fetched from API and saved to data variable.
  const [loading, setLoading] = React.useState(true);
  // list variable is to save details of a particular role to display in UI.
  let list = [];
  if (loading === false) {
    list = [{ title: "Role: ", value: data.role }];
  }

  // useEffect to get data from API
  useEffect(
    () => {
      if (roleId !== null) {
        const url = `/role/${roleId}`;
        // get a particular role data from API
        getData(url).then((res) => {
          setData(res.data.data);
          setLoading(false);
        });
      }
    },
    [roleId],
  );

  return (
    <>
      { (loading) ? <div className="rightLoader"><CircularProgress size={50} style={{ color: "#8A2BE2" }} /></div>
        : (
          <Grid>
            <Paper elevation={0}>
              <Grid container spacing={1}>

                {// details of particular role would be display through mapping of list variable
                list.map((item, index = 1) => (
                  <Grid container xs={12} key={index} item>
                    <Grid xs={6} item>
                      <span
                        style={{
                          fontFamily: 'Helvetica',
                          fontStyle: "normal",
                          fontWeight: "400",
                          fontSize: "13px",
                          margin: "20px",
                        }}
                      >
                        {/* className="heading" */}
                        {item.title }

                      </span>
                    </Grid>
                    <Grid xs={6} item>
                      <span
                        style={{
                          fontFamily: 'Helvetica',
                          fontStyle: "normal",
                          fontWeight: "400",
                          fontSize: "13px",
                          margin: "20px",
                        }}
                      >
                        {/* className="value" */}
                        {item.value}

                      </span>
                    </Grid>
                  </Grid>
                ))
}
              </Grid>
            </Paper>
          </Grid>
        )}
    </>
  );
}
