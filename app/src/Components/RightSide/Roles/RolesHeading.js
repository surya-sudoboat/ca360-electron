// import React from 'react';
// import { Typography, Button } from '@material-ui/core';
// import { useDispatch } from 'react-redux';
// import {
//   rolePageState, ChangeTab,
// } from '../../../actions/index';
// import { getCookie } from "../../cookie/Index";

// export default function ClientHeading() {
//   // variable to check login user role
//   const isAdmin = getCookie('isAdmin') === 'true';
//   // variable for dispatch an action
//   const dispatch = useDispatch();
//   // Variable for create Style create new Button
//   const button = {
//     backgroundColor: '#2c4e86',
//     color: 'white',
//     marginRight: 30,
//     padding: 10,
//   };
//   // function to handle CREATE button click
//   const handleClick = () => {
//     // to show the new role form
//     dispatch(rolePageState("roleCreate"));
//     dispatch(ChangeTab('1'));
//   };
//   return (
//     <div className="Subheading">
//       <Typography variant="h5">Roles</Typography>
//       {(isAdmin)
//         ? (
//           <Button style={button} onClick={handleClick}>
//             Create
//           </Button>
//         // eslint-disable-next-line react/jsx-no-useless-fragment
//         ) : <></>}
//     </div>
//   );
// }

import React from 'react';
import { useDispatch } from 'react-redux';
import { Button } from '@material-ui/core';
import PersonIcon from '@mui/icons-material/Person';
import HeaderNew from "../../Header/HeaderNew";
import {
  rolePageState, ChangeTab,
} from '../../../actions/index';
import { getCookie } from "../../cookie/Index";

export default function RolesHeading() {
  // variable to check login user role
  const isAdmin = getCookie('isAdmin') === 'true';
  const dispatch = useDispatch();
  const button = {
    backgroundColor: '#8A2BE2',
    color: 'white',
    marginLeft: " 88%",
    padding: 10,
  };
  const handleClick = () => {
    dispatch(rolePageState("roleCreate"));
    dispatch(ChangeTab('1'));
  };
  return (
    <>
      <HeaderNew heading="Roles" />
      {(isAdmin)
        ? (
          <Button
            style={button}
            onClick={() => {
              handleClick();
            }}
          >
            <PersonIcon style={{ marginRight: "4px" }} />
            Add Role
          </Button>
        ) : null}
    </>
  );
}
