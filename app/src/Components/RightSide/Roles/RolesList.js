import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux';
// import { Button } from "@material-ui/core"; // Typography
import { Paper } from "@material-ui/core";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import CircularProgress from '@mui/material/CircularProgress';
import { toast, ToastContainer } from "react-toastify";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { rolePageState, roleIdChange, reload } from "../../../actions/index";
import { getData, deleteData } from "../../Network/Index";
import Popup from "../../PopUp/PopUp";

export default function RolesList() {
  const currentState = useSelector((state) => state.rolePageState);
  // list variable is to save all roles
  const [list, setList] = React.useState([]);
  // eslint-disable-next-line max-len
  // variable isLoading would be false only if data of a particular role is fetched from API and saved to list variable
  const [isLoading, setIsLoading] = React.useState(true);
  const [showPopup, setShowPopup] = useState(false);
  // variable to get data from store
  const reloadPage = useSelector((state) => state.reloadPage);
  const roleId = useSelector((state) => state.roleIdChange);
  const search = useSelector((state) => state.search);
  const searchItem = useSelector((state) => state.searchValue);
  // Variables for styling
  // const listContent = { color: '#ffffff', textAlign: 'center' };
  const listContent = {
    color: '#161616',
    textAlign: 'center',
    fontFamily: 'Helvetica',
    fontStyle: "normal",
    fontWeight: "400",
    fontSize: "13px",
  };
  const listTopic = {
    color: '#ffffff',
    textAlign: 'center',
    backgroundColor: "#8A2BE2",
    fontFamily: 'Helvetica',
    fontStyle: "normal",
    fontWeight: "700",
    fontSize: "13px",
  };
  // const listbtn = { color: "#ffffff", backgroundColor: "#2c4e86" };
  // const loaderStyle = { color: "#8A2BE2" };
  // variable for dispatch an action
  const dispatch = useDispatch();

  // function to handle view details of roles
  const handleView = (id) => {
    dispatch(roleIdChange(id));

    dispatch(rolePageState("roleView"));
  };

  // function called when edit icon is clicked
  const handleEdit = (id) => {
    // to show the edit role form
    dispatch(rolePageState("roleEdit"));
    dispatch(roleIdChange(id));
  };

  // function called when delete icon is clicked
  const handleDelete = () => {
    const url = `/role/${roleId}`;
    // eslint-disable-next-line no-unused-vars
    deleteData(url).then((res) => {
      toast.error(res.data.message);
      // to show the details of first role in the list after deleting any role
      //  dispatch((roleIdChange(null)));
      dispatch(reload());
      setShowPopup(false);
    }).catch((error) => {
      toast.error(error.response.data);
    });
  };

  // function to get list of all the roles
  useEffect(() => {
    const url = "/role/";

    getData(url).then((res) => {
      setIsLoading(true);
      if (search) {
        setList(res.data.data.filter((item) => item.role.includes(searchItem)));
      } else {
        setList(res.data.data);
      }
      // if (roleId === null) {
      //   dispatch(roleIdChange(res.data.data[0]._id));
      // } else {
      //   dispatch(roleIdChange(roleId));
      // }
      setIsLoading(false);
    })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  }, [reloadPage, searchItem]);

  return (

    <Paper elevation={3} className="listContainer">
      {/* <Typography variant="h4" className="listHeading">
        List of Roles
      </Typography> */}

      <div className="listTableContainer">
        <TableContainer style={{ maxHeight: "100%" }}>
          <Table stickyHeader aria-label="a dense table">
            <TableHead style={{ color: "white" }}>
              <TableRow>
                <TableCell style={listTopic}>S.No</TableCell>
                <TableCell style={listTopic}>Role</TableCell>
                {(currentState !== "view" && currentState !== "create" && currentState !== "edit") ? <TableCell style={listTopic}>Actions</TableCell> : null }
                <TableCell style={listTopic}> </TableCell>
              </TableRow>
            </TableHead>
            { (!isLoading)
              ? (
                <TableBody style={{ overflowY: "revert" }}>
                  {// all roles will be shown in a list through the mapping of list variable
                  list.map((row, index = 1) => (
                    <TableRow key={index} style={{ cursor: "pointer" }} onClick={() => { handleView(row._id); }}>
                      <TableCell style={listContent}>{ index + 1 }</TableCell>
                      <TableCell style={listContent}>{row.role}</TableCell>
                      {(currentState !== "view" && currentState !== "create" && currentState !== "edit")
                        ? (
                          <TableCell style={listContent}>

                            <EditIcon
                              onClick={(e) => {
                                e.stopPropagation();
                                handleEdit(row._id);
                              }}
                              style={{ pointer: "cursor", color: "#7E7E7E" }}
                            />

                            <DeleteIcon
                              onClick={(e) => {
                                e.stopPropagation();
                                setShowPopup(true);
                                dispatch(roleIdChange(row._id));
                                //  handleDelete(row._id);
                              }}
                              style={{ marginLeft: "20px", pointer: "cursor", color: "#7E7E7E" }}
                            />

                          </TableCell>
                        ) : null }
                      <TableCell>
                        { /*
                        <Button style={listbtn} onClick={() => { handleView(row._id); }}>
                          View Detail
                        </Button>
                   */}
                        {' '}

                      </TableCell>
                    </TableRow>
                  ))
}
                </TableBody>
              ) : (
                <TableBody>
                  <TableRow>
                    <TableCell colSpan={5} style={{ borderBottom: "none" }}>
                      <CircularProgress size={50} style={{ margin: "16% 45%", color: "#8A2BE2" }} />
                    </TableCell>
                  </TableRow>
                </TableBody>
              )}
          </Table>
        </TableContainer>
        <ToastContainer />
        {showPopup ? (

          <Popup
            handleClose={() => { setShowPopup(false); }}
            handleDelete={() => { handleDelete(); }}
            handleCancel={() => { setShowPopup(false); }}
          />

        ) : null}
      </div>

    </Paper>

  );
}
