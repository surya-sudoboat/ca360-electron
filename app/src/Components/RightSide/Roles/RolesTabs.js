/* eslint-disable import/no-unresolved */
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { ToastContainer, toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import { TabContext, TabList, TabPanel } from '@material-ui/lab';
import { Button, Paper } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import RolesInfoList from "./RolesInfoList";
import { rolePageState, reload, roleIdChange } from "../../../actions/index";
import { deleteData } from "../../Network/Index";
import NewRoleForm from "../../Forms/RoleForm/NewRoleForm";
import EditRoleForm from "../../Forms/RoleForm/EditRoleForm";
import { getCookie } from "../../cookie/Index";
import Popup from "../../PopUp/PopUp";

export default function LabTabs() {
  const [displayIcon, setDisplayIcon] = useState(true);
  const [showPopup, setShowPopup] = useState(false);
  // variable to get data from store
  const currentState = useSelector((state) => state.rolePageState);
  const roleId = useSelector((state) => state.roleIdChange);

  // variable for dispatch an action
  const dispatch = useDispatch();
  // variable to check login user role
  const isAdmin = getCookie('isAdmin') === 'true';

  // function called when edit icon is clicked
  const handleEdit = (e) => {
    e.preventDefault();
    // to show the edit role form
    dispatch(rolePageState("roleEdit"));
  };

  // function called when delete icon is clicked
  const handleDelete = () => {
    const url = `/role/${roleId}`;
    // eslint-disable-next-line no-unused-vars
    deleteData(url).then((res) => {
      toast.error(res.data.message);
      // to show the details of first role in the list after deleting any role
      dispatch((roleIdChange(null)));
      dispatch(reload());
      setShowPopup(false);
    }).catch((error) => {
      toast.error(error.response.data);
    });
  };

  return (
    <Paper elevation={3} className="clienttab">
      <TabContext value="1">
        <Box className="tabs">
          <TabList aria-label="lab API tabs example">
            <Tab label="Role Information" value="1" />
            <Box style={{ marginLeft: "32%" }}>
              { displayIcon && (isAdmin)
                ? <Button onClick={handleEdit} style={{ color: "white", padding: 12 }}><EditIcon /></Button>
              // eslint-disable-next-line react/jsx-no-useless-fragment
                : null }

              {displayIcon && (isAdmin)
                ? (
                  <Button
                    onClick={() => {
                      setShowPopup(true);
                      //   handleDelete();
                    }}
                    style={{ color: "white", padding: 12 }}
                  >
                    <DeleteIcon />
                  </Button>
                )
              // eslint-disable-next-line react/jsx-no-useless-fragment
                : null }

            </Box>
          </TabList>
        </Box>
        { currentState === "view" && (
          <Box style={{ height: "92%", overflowY: "scroll" }}>
            {!displayIcon && (setDisplayIcon(true))}
            <TabPanel value="1"><RolesInfoList /></TabPanel>

          </Box>
        )}
        { currentState === "create" && (
          <Box style={{ height: "92%", overflowY: "scroll" }}>
            {displayIcon && (setDisplayIcon(false))}
            <TabPanel value="1"><NewRoleForm /></TabPanel>

          </Box>
        )}
        { currentState === "edit" && (
          <Box style={{ height: "92%", overflowY: "scroll" }}>
            {displayIcon && (setDisplayIcon(false))}
            <TabPanel value="1">
              <EditRoleForm />
              {' '}
            </TabPanel>
          </Box>
        )}
      </TabContext>
      <ToastContainer />
      {showPopup ? (

        <Popup
          handleClose={() => { setShowPopup(false); }}
          handleDelete={() => { handleDelete(); }}
          handleCancel={() => { setShowPopup(false); }}
        />

      ) : null}
    </Paper>
  );
}
