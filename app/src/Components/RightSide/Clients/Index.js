import React from 'react';
import { useSelector } from "react-redux";
import ClientHeading from './ClientHeading';
import ClientsList from './ClientsList';
import ClientsTabs from './ClientsTabs';

// Function for show Clients Page
function ClientsPage() {
  const clientPageState = useSelector((state) => state.clientPageState);

  return (
    <div className="rightSide">
      <div className="ClientSubheading">
        <ClientHeading />
      </div>
      <div className="clientContainer">
        <ClientsList />
        {(clientPageState === "viewClient" || clientPageState === "createClient" || clientPageState === "editClient")
          ? <ClientsTabs /> : null}
      </div>
    </div>
  );
}

export default ClientsPage;
