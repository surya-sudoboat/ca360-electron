import React, { useState } from 'react';
import { Button, Typography } from '@material-ui/core'; // Typography, TextField,
import { useDispatch } from 'react-redux';
// import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import PersonIcon from '@mui/icons-material/Person';
import FormControlLabel from "@mui/material/FormControlLabel";
import Switch from "@mui/material/Switch";
import {
  createClient, ChangeTab, resetClinetInfo, showGst,
} from '../../../actions/index';
import HeaderNew from "../../Header/HeaderNew";

export default function ClientHeading() {
  // variable for dispatch
  const dispatch = useDispatch();
  // Variable for create Style create new Button
  const button = {
    backgroundColor: '#8A2BE2',
    color: 'white',
    marginRight: "0.6%",
    padding: 10,
  };
  // Function to handle create new button click
  const handleClick = () => {
    dispatch(createClient());
    dispatch(ChangeTab('1'));
    dispatch(resetClinetInfo());
  };
  const [gstState, setGstState] = useState(false);
  dispatch(showGst(gstState));
  return (
    <>
      <HeaderNew heading="Clients" />

      <div className="Subheading">
        <FormControlLabel
          style={{ float: 'right' }}
          control={(
            <Switch
              checked={gstState}
              onChange={(e) => { setGstState(e.target.checked); }}
            />
              )}
          label="Show GST Clients"
        />
        <Button
          style={button}
          onClick={() => {
            handleClick();
          }}
        >
          <PersonIcon style={{ marginRight: "4px" }} />
          <Typography style={{
            fontFamily: "Helvetica",
            fontStyle: "normal",
            fontWeight: "700",
            fontSize: "13px",
            textTransform: "none",
          }}
          >
            Add Client
          </Typography>
        </Button>
      </div>
    </>
  );
}
