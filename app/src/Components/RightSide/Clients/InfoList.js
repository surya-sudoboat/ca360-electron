import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Grid, Box, Typography } from '@material-ui/core';
import CircularProgress from '@mui/material/CircularProgress';
import { ToastContainer, toast } from 'react-toastify';
import { getData } from '../../Network/Index';

function InfoList({ type }) {
  // variable to get data from Api of particular client
  const [data, setData] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  // variable to get data from store
  const clientID = useSelector((state) => state.clientIDchange);
  // Variable to map dats for display in different tab.
  let basicList = [];
  let loginList = [];
  // IF data is not empty and get through API
  if (!loading) {
    // It contains basic information of client to display in basic tabs.
    basicList = [
      { tittle: 'Client ID :', value: data.uniqueId },
      { tittle: 'Basic Details', value: '' },
      { tittle: 'Business Name :', value: data.businessName },
      { tittle: 'Responsible Person :', value: data.dependent },
      { tittle: 'DOB / DOI :', value: data.dateOfBirth.substring(0, 10) },
      {
        tittle: 'Date of Commencement :',
        value: data.startDate.substring(0, 10),
      },
      { tittle: 'Aadhaar No. :', value: data.aadhaar },
      { tittle: 'Entity Status :', value: data.entityType },
      {
        tittle: 'Nature of Business :',
        value: data.natureOfBusiness.join(', '),
      },
      { tittle: 'Notes :', value: data.notes },
      { tittle: 'Contact Details', value: '' },
      { tittle: 'Primary Mobile No. :', value: data.contacts.primaryMobile },
      {
        tittle: 'Secondary Mobile No. :',
        value: data.contacts.secondaryMobile,
      },
      { tittle: 'Primary EmailID :', value: data.contacts.primaryMail },
      { tittle: 'Secondary EmailID :', value: data.contacts.secondaryMail },
      { tittle: 'Address :', value: data.contacts.address },
      { tittle: 'WhatsApp To :', value: data.contacts.whatsappTo },
      { tittle: 'WhatsApp Group :', value: data.contacts.whatsappGroupType },
    ];

    // It contains login information of client to display in login tab .
    loginList = [
      { tittle: 'Login Details of Income Tax', value: '' },
      { tittle: 'Username-IT :', value: data.it.username },
      { tittle: 'Password-IT :', value: data.it.password },
      { tittle: 'Login Details of TDS', value: '' },
      { tittle: 'TAN :', value: data.tds.tanNumber },
      { tittle: 'Username-IT Portal :', value: data.tds.itUsername },
      { tittle: 'Password-IT Portal :', value: data.tds.itPassword },
      { tittle: 'Username-Traces :', value: data.tds.tracesUsername },
      { tittle: 'Password-Traces :', value: data.tds.tracesPassword },
    ];
    if (data.gst !== undefined) {
      loginList.push({ tittle: 'Login Details of GST', value: '' });
      loginList.push({ tittle: 'GSTIN :', value: data.gst.gstNumber });
      loginList.push({ tittle: 'Username-GST :', value: data.gst.gstUsername });
      loginList.push({ tittle: 'Password-GST :', value: data.gst.gstPassword });
      loginList.push({ tittle: 'Username-E-way Bill :', value: data.gst.ewayUsername });
      loginList.push({ tittle: 'Password-E-way Bill :', value: data.gst.ewayPassword });
      loginList.push({ tittle: 'Filing Type :', value: data.gst.filingType });
      loginList.push({ tittle: 'Registered State :', value: data.gst.registeredState });
      loginList.push({ tittle: 'DSC / EVS :', value: data.gst.verificationCode });
      loginList.push({ tittle: 'Payment Mode :', value: data.gst.paymentMode });
    }
  }

  // useEffect to get data from API
  useEffect(() => {
    if (clientID !== '1') {
      const url = `/client/${clientID}`;
      // get A client data from API
      getData(url)
        .then((res) => {
          if (res.data.data._id !== undefined) {
            setData(res.data.data);
            setLoading(false);
          }
        })
        .catch((error) => {
          toast.error(error.response.data.message);
        });
    }
  }, [clientID]);

  // function to display data in different tab it display in basic info tab.
  if (type === 'basic') {
    return (
      <Grid>
        {!loading ? (
          <Box>
            <Grid container spacing={1}>
              {/* Mapping of all of basic info store in variable */}
              {basicList.map((list, index = 1) => {
              // if value is empty then display a strip with lable
                if (list.value === '') {
                  return (
                    <Grid xs={12} key={index} item>
                      <Box sx={{
                        padding: "10px 30px", border: '1px solid #8A2BE2', width: "30%", margin: "10px auto",
                      }}
                      >
                        <Typography style={{ color: "#8A2BE2", textAlign: "center" }}>
                          {list.tittle}
                        </Typography>
                      </Box>
                    </Grid>
                  );
                }
                // if value is not empty then display value with attribute
                return (
                  <Grid container xs={12} item key={index}>
                    <Grid xs={6} item>
                      <Box
                        component="span"
                        style={{
                          fontFamily: "Helvetica",
                          fontStyle: "normal",
                          fontWeight: 400,
                          fontSize: "13px",
                          lineHeight: "15px",
                          letterSpacing: "0.03em",
                        }}
                      >
                        {list.tittle}
                      </Box>
                    </Grid>
                    <Grid xs={6} item>
                      <Box
                        component="span"
                        style={{
                          fontFamily: "Helvetica",
                          fontStyle: "normal",
                          fontWeight: 400,
                          fontSize: "13px",
                          lineHeight: "15px",
                          letterSpacing: "0.03em",
                        }}
                      >
                        {list.value}
                      </Box>
                    </Grid>
                  </Grid>
                );
              })}
            </Grid>
          </Box>
        ) : (
          <div className="rightLoader">
            <CircularProgress size={50} style={{ color: "#8A2BE2" }} />
          </div>
        )}
        <ToastContainer />
      </Grid>
    );
  }

  // function to display data in different tab it display in login info tab.
  return (
    <Grid>
      {!loading ? (
        <Box>
          <Grid container spacing={1}>
            {/* Mapping of all of login info store in variable with their attribute */}
            {loginList.map((list, index = 1) => {
            // if value is empty then display a strip with lable
              if (list.value === '') {
                return (
                  <Grid xs={12} key={index} item style={{ paddingTop: 0 }}>
                    <Box sx={{
                      padding: "10px 30px", border: '1px solid #8A2BE2', width: "45%", margin: "10px auto",
                    }}
                    >
                      <Typography style={{ color: "#8A2BE2", textAlign: "center" }}>
                        {list.tittle}
                      </Typography>
                    </Box>
                  </Grid>
                );
              }
              // if value is not empty then display value with attribute
              return (
                <Grid container xs={12} item key={index}>
                  <Grid xs={6} item>
                    <Box
                      component="span"
                      style={{
                        fontFamily: "Helvetica",
                        fontStyle: "normal",
                        fontWeight: 400,
                        fontSize: "13px",
                        lineHeight: "15px",
                        letterSpacing: "0.03em",
                      }}
                    >
                      {list.tittle}

                    </Box>
                  </Grid>
                  <Grid xs={6} item>
                    <Box
                      component="span"
                      style={{
                        fontFamily: "Helvetica",
                        fontStyle: "normal",
                        fontWeight: 400,
                        fontSize: "13px",
                        lineHeight: "15px",
                        letterSpacing: "0.03em",
                      }}
                    >
                      {list.value}
                    </Box>
                  </Grid>
                </Grid>
              );
            })}
          </Grid>
        </Box>
      ) : (
        <div className="rightLoader">
          <CircularProgress size={50} style={{ color: "white" }} />
        </div>
      )}
      <ToastContainer />
    </Grid>
  );
}

export default InfoList;
