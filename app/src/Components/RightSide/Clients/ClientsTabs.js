/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import { TabContext, TabList, TabPanel } from '@material-ui/lab';
import { Button, Paper } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import BasicInfoForm from '../../Forms/ClientForm/BasicInfoForm';
import LoginCredForm from '../../Forms/ClientForm/LoginCredForm';
import InfoList from './InfoList';
import {
  editClient, reload, ChangeTab, saveClinetInfo, clientIDchange,
} from '../../../actions/index';
import { deleteData, getData } from '../../Network/Index';
import { getCookie } from '../../cookie/Index';
import Popup from "../../PopUp/PopUp";

export default function LabTabs() {
  // variable to check login user role
  const isAdmin = getCookie('isAdmin') === 'true';
  const [displayIcon, setDisplayIcon] = useState(true);
  const [showPopup, setShowPopup] = useState(false);
  // variable to get data from store
  const value = useSelector((state) => state.CurrentTab);
  const currentState = useSelector((state) => state.clientPageState);
  const clientID = useSelector((state) => state.clientIDchange);
  // variable for dispatch action
  const dispatch = useDispatch();
  // variable for styling
  const icon = { color: 'white', padding: 12 };

  // function to handle tab change
  const handleChange = (event, newValue) => {
    dispatch(ChangeTab(newValue));
  };

  // function to handle edit button click
  const handleEdit = () => {
    // Get data from server of current client to edit.
    const url = `/client/${clientID}`;
    getData(url)
      .then((res) => {
        const editValues = {
          ...res.data.data,
          dateOfBirth: res.data.data.dateOfBirth.substring(0, 10),
          startDate: res.data.data.startDate.substring(0, 10),
        };
        dispatch(ChangeTab('1'));
        dispatch(saveClinetInfo(editValues));
        dispatch(editClient());
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  };

  // function to handle delete button click allow only admin role
  const handleDelete = () => {
    const url = `/client/${clientID}`;
    // delete request to delete client
    deleteData(url)
      .then((res) => {
        toast.error(res.data.message);
        if (isAdmin) {
          dispatch(clientIDchange('1'));
        }
        dispatch(reload());
        setShowPopup(false);
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  };

  return (
    // Code for Material UI Tabs
    <Paper elevation={3} className="clienttab">
      <TabContext value={value}>
        <Box className="tabs">
          <TabList onChange={handleChange}>
            {/* List of two tab */}
            <Tab label="Basic Info" value="1" />
            <Tab label="Login Credentials" value="2" />
            {/* Edit and delete Icons */}
            <Box style={{ marginLeft: '15%' }}>
              {/* marginleft:"18%" */}
              {displayIcon && (
              <div>
                <Button onClick={handleEdit} style={icon}>
                  <EditIcon />
                </Button>
                  {/* <Button  onClick={handleDelete}style={icon}> */}
                <Button
                  onClick={() => {
                    setShowPopup(true);
                  }}
                  style={icon}
                >

                  <DeleteIcon />
                </Button>
              </div>
              )}
            </Box>
          </TabList>
        </Box>
        {/* If user want see detail of client by default this page is open */}
        {currentState === 'viewClient' && (
          <Box className="tab">
            {!displayIcon && (setDisplayIcon(true))}
            <TabPanel value="1">
              <InfoList type="basic" />
            </TabPanel>
            <TabPanel value="2">
              <InfoList type="login" />
            </TabPanel>
          </Box>
        )}
        {/* If user Want to create new user Forms will open in two tabs */}
        {currentState === 'createClient' && (
          <Box className="tab">
            {displayIcon && (setDisplayIcon(false))}
            <TabPanel value="1">
              <BasicInfoForm />
            </TabPanel>
            <TabPanel value="2">
              <LoginCredForm mode="create" />
              {/* <Form type="login" /> */}
            </TabPanel>
          </Box>
        )}
        {/* If user want to Edit clients info Form will open with fill info */}
        {currentState === 'editClient' && (
          <Box className="tab">
            {displayIcon && (setDisplayIcon(false))}
            <TabPanel value="1">
              <BasicInfoForm />
            </TabPanel>
            <TabPanel value="2">
              <LoginCredForm type="login" mode="edit" />
              {/* <Form type="login" editValues="edit" clientID={clientID} /> */}
            </TabPanel>
          </Box>
        )}
      </TabContext>
      <ToastContainer />
      {showPopup ? (

        <Popup
          handleClose={() => { setShowPopup(false); }}
          handleDelete={() => { handleDelete(); }}
          handleCancel={() => { setShowPopup(false); }}
        />

      ) : null}
    </Paper>
  );
}
