/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { Button } from '@material-ui/core';
import {
  Paper, Typography, Box, Button, TextField,
} from "@material-ui/core";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import CircularProgress from "@mui/material/CircularProgress";
// import FormControlLabel from "@mui/material/FormControlLabel";
// import Switch from "@mui/material/Switch";
import { toast, ToastContainer } from "react-toastify";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import CloseIcon from '@mui/icons-material/Close';
import Popup from "../../PopUp/PopUp";
import {
  clientIDchange,
  viewClient,
  editClient,
  ChangeTab,
  saveClinetInfo,
  reload,
  searchValue,
} from "../../../actions/index";
import { deleteData, getData } from "../../Network/Index";
import gstIcon from "../../../Images/GstIcon.png";
import { getCookie } from '../../cookie/Index';

export default function ClientsList() {
  // variable to check login user role
  const isAdmin = getCookie('isAdmin') === 'true';
  // variable to get data from store
  const reloadPage = useSelector((state) => state.reloadPage);
  const clientID = useSelector((state) => state.clientIDchange);
  // variable to update data after getting throgh api
  const [list, setList] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [showPopup, setShowPopup] = useState(false);
  // const [search, setSearch] = useState(false);
  // const [searchValue, setSearchValue] = useState(null);
  const search = useSelector((state) => state.search);
  const searchItem = useSelector((state) => state.searchValue);
  // const [deleteItem, setDeleteItem] = useState(false);
  // const [gstState, setGstState] = useState(false);
  const gstState = useSelector((state) => state.showGst);
  //
  const currentState = useSelector((state) => state.clientPageState);
  // Variables for styling
  // const listContent = { color: '#ffffff', textAlign: 'center' };
  const listContent = {
    color: "#161616",
    textAlign: "left",
    fontFamily: "Helvetica",
    fontStyle: "normal",
    fontWeight: "400",
    fontSize: "13px",
  };
  const listTopic = {
    color: "#ffffff",
    textAlign: "left",
    backgroundColor: "#8A2BE2",
    fontFamily: "Helvetica",
    fontStyle: "normal",
    fontWeight: "700",
    fontSize: "13px",
  };
  // const listbtn = { color: '#ffffff', backgroundColor: '#2c4e86' };

  // const loaderStyle = { color: "#8A2BE2" };
  const gstIconStyle = {
    marginLeft: "5px",
    height: "25px",
    width: "25px",
    borderRadius: "50%",
  };
  // variable for dispatch
  const dispatch = useDispatch();
  // function to handle view details of client
  const handleView = (id) => {
    dispatch(clientIDchange(id));
    dispatch(viewClient());
  };

  const handleEdit = (clientId) => {
    // dispatch(viewClient());

    // Get data from server of current client to edit.
    const url = `/client/${clientId}`;
    getData(url)
      .then((res) => {
        const editValues = {
          ...res.data.data,
          dateOfBirth: res.data.data.dateOfBirth.substring(0, 10),
          startDate: res.data.data.startDate.substring(0, 10),
        };
        dispatch(ChangeTab("1"));
        dispatch(saveClinetInfo(editValues));
        dispatch(editClient());
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  };

  // function to handle delete button click allow only admin role
  const handleDelete = () => {
    // setDeleteItem(true);
    // console.log(deleteItem);

    const url = `/client/${clientID}`;
    // delete request to delete client
    deleteData(url)
      .then((res) => {
        toast.error(res.data.message);
        // if (isAdmin) {
        //   dispatch(clientIDchange('1'));
        // }
        dispatch(viewClient(null));
        dispatch(reload());
        setShowPopup(false);
        //   setDeleteItem(false);
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  };

  // function to get list of all the clients
  useEffect(() => {
    setIsLoading(true);
    if (!gstState) {
      const url = "/client";
      getData(url)
        .then((res) => {
          // handle view by default display first client
          // if (clientID === "1") {
          //   dispatch(clientIDchange(res.data.data[0]._id));
          // }
          if (search) {
            setList(res.data.data.filter((item) => item.dependent.includes(searchItem)
            || item.businessName.includes(searchItem)));
          } else {
            setList(res.data.data);
          }
          // update list after getting data from api
          // setList(res.data.data.filter((item) =>
          // item.dependent === 'Saurabh ' || item.businessName === "business 20"));
          setIsLoading(false);
        })
        .catch((error) => {
          toast.error(error.response.data.message);
        });
    } else {
      const url = "/client?type=gst";
      getData(url)
        .then((res) => {
          // handle view after adding new Client
          dispatch(clientIDchange(res.data.data[0]._id));
          // update list after getting data from api
          if (search) {
            setList(res.data.data.filter((item) => item.dependent.includes(searchItem)
            || item.businessName.includes(searchItem)));
          } else {
            setList(res.data.data);
          }
          setIsLoading(false);
        })
        .catch((error) => {
          toast.error(error.response.data.message);
        });
    }
  }, [reloadPage, gstState, searchItem]);

  // // function to handle view details of client
  // const handleView = (id) => {
  //   dispatch(clientIDchange(id));
  //   dispatch(viewClient());
  // };
  return (
    <Paper elevation={3} className="listContainer">
      {/* <Typography variant="h4" className="listHeading">
        List of Clients
        <FormControlLabel
          style={{ float: 'right' }}
          control={(
            <Switch
              checked={gstState}
              onChange={(e) => { setGstState(e.target.checked); }}
            />
              )}
          label="Show GST Clients"
        />

      </Typography>
       */}
      {/* Wait Untill get response */}
      {/*  {(!isLoading) ? (  */}

      <div className="listTableContainer">
        <TableContainer style={{ maxHeight: "100%" }}>
          { (!isLoading && list.length === 0) ? (<Typography variant="h4" style={{ textAlign: "center" }}>No record found!</Typography>)
            : (
              <Table stickyHeader aria-label="a dense table">
                {/* table headings */}
                <TableHead style={{ height: "10px", color: "white" }}>
                  <TableRow>
                    <TableCell style={listTopic}>S.No</TableCell>
                    <TableCell style={listTopic}>Business Name</TableCell>
                    <TableCell style={listTopic}>Person Name</TableCell>
                    <TableCell style={listTopic}>Primary Mobile Number</TableCell>
                    {(currentState !== "viewClient" && currentState !== "createClient" && currentState !== "editClient") ? <TableCell style={listTopic}>Actions</TableCell> : null }
                  </TableRow>
                </TableHead>
                {/* table data */}
                {
            // eslint-disable-next-line no-nested-ternary
            !isLoading ? (

              <TableBody style={{ overflowY: "scroll" }}>
                {
                  list.map((row, index = 1) => (
                    <TableRow
                      key={index}
                      style={{ cursor: "pointer" }}
                      onClick={() => {
                        handleView(row._id);
                      }}
                    >
                      <TableCell style={listContent}>
                        <div style={{ display: "flex" }}>
                          {index + 1}
                          {" "}
                          {!gstState && row.gst ? (
                            <img
                              style={gstIconStyle}
                              src={gstIcon}
                              alt="gstIcon"
                            />
                          ) : null}
                        </div>
                      </TableCell>
                      <TableCell style={listContent}>
                        {row.businessName}
                      </TableCell>
                      <TableCell style={listContent}>{row.dependent}</TableCell>
                      <TableCell style={listContent}>
                        {row.contacts.primaryMobile}
                      </TableCell>
                      {(currentState !== "viewClient" && currentState !== "createClient" && currentState !== "editClient")
                        ? (
                          <TableCell style={listContent}>

                            <EditIcon
                              onClick={(e) => {
                                handleEdit(row._id);
                              }}
                              style={{ pointer: "cursor", color: "#7E7E7E" }}
                            />

                            <DeleteIcon
                              onClick={(e) => {
                                e.stopPropagation();
                                setShowPopup(true);
                                dispatch(clientIDchange(row._id));
                              //   handleDelete();
                              }}
                              style={{ marginLeft: "20px", pointer: "cursor", color: "#7E7E7E" }}
                            />

                          </TableCell>
                        ) : null }
                    </TableRow>
                  ))
}
              </TableBody>

            ) : (
              <TableBody>
                <TableRow>
                  <TableCell colSpan={5} style={{ borderBottom: "none" }}>
                    <CircularProgress
                      size={50}
                      style={{ margin: "16% 45%", color: "#8A2BE2" }}
                    />
                  </TableCell>
                </TableRow>
              </TableBody>
            )
}
              </Table>
            )}
        </TableContainer>
        <ToastContainer />
        {showPopup ? (
          <>
            {/*
          <div className="popup-box">
            <div className="box">
              <span className="close-icon">
              <CloseIcon onClick={() => { setShowPopup(false); }} />
              </span>
              <h4>Are you sure to delete this file?</h4>
              <h6>If you delete this file you can;t recover it.</h6>
              <div style={{ display: "flex", marginLeft: "50%" }}>
                <Button onClick={() => { setShowPopup(false); }}>Cancel</Button>
                <Button onClick={() => {
                  // setDeleteItem(true);
                  // console.log(`button${deleteItem}`);
                  handleDelete();
                }}
                >
                  Delete
                </Button>
              </div>
            </div>
            </div>
              */}
            <Popup
              handleClose={() => { setShowPopup(false); }}
              handleDelete={() => { handleDelete(); }}
              handleCancel={() => { setShowPopup(false); }}
            />

          </>
        ) : null}

      </div>
      {/*   ) : (
        <div className="leftLoader">
          <CircularProgress size={50} style={loaderStyle} />
        </div>
  )}  */}
    </Paper>
  );
}
