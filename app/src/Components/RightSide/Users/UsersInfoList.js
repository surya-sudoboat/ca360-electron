/* eslint-disable react/jsx-no-useless-fragment */
/* eslint-disable react/style-prop-object */
import React, { useEffect } from "react";
import { Grid, Paper } from '@material-ui/core';
import CircularProgress from '@mui/material/CircularProgress';
import { useSelector } from "react-redux";
import { getData } from "../../Network/Index";

export default function UsersInfoList() {
  // variable to get data from store
  const userId = useSelector((state) => state.userIdChange);
  // data variable is to get details from Api of a particular user.
  const [data, setData] = React.useState({});
  // eslint-disable-next-line max-len
  // variable loading would be false only if data of a particular user is fetched from API and saved to data variable.
  const [loading, setLoading] = React.useState(true);
  // list variable is to save details of a particular user to display in UI.
  let list = [];
  if (loading === false) {
    list = [{ title: "User ID: ", value: data.uniqueId }, { title: "Name: ", value: data.name }, { title: "Username: ", value: data.username }, { title: "Role: ", value: data.role }];
  }
  // useEffect to get data from API
  useEffect(() => {
    if (userId !== null) {
      const url = `/user/${userId}`;
      // get a particular user data from API
      getData(url).then((res) => {
        console.log(res);
        setData(res.data.data);
        setLoading(false);
      });
    }
  }, [userId]);

  return (
    <>
      { (loading) ? <div className="rightLoader"><CircularProgress size={50} style={{ color: "#8A2BE2" }} /></div>
        : (
          <Grid>
            <Paper elevation={0}>
              <Grid container spacing={1}>
                {// details of particular user would be display through mapping of list variable
                list.map((item, index = 0) => (
                  <Grid container xs={12} key={index} item>
                    <Grid xs={6} item>
                      <span style={{
                        fontFamily: 'Helvetica',
                        fontStyle: "normal",
                        fontWeight: "400",
                        fontSize: "13px",
                        margin: "20px",
                      }}
                      >
                        {item.title }

                      </span>
                      {/* className="heading" */}
                    </Grid>
                    <Grid xs={6} item>
                      <span style={{
                        fontFamily: 'Helvetica',
                        fontStyle: "normal",
                        fontWeight: "400",
                        fontSize: "13px",
                        margin: "20px",
                      }}
                      >
                        {item.value}

                      </span>
                      {/* className="value" */}
                    </Grid>
                  </Grid>
                ))
}
              </Grid>
            </Paper>
          </Grid>
        )}
    </>
  );
}
