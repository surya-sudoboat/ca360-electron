/* eslint-disable import/no-named-as-default */
/* eslint-disable import/no-named-as-default-member */

/* eslint-disable import/no-unresolved */
import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import { ToastContainer, toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import { TabContext, TabList, TabPanel } from '@material-ui/lab';
import { Button, Paper } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import UsersInfoList from "./UsersInfoList";
import {
  userPageState, reload, userIdChange, // saveUserInfo,
} from "../../../actions/index";
import { deleteData, getData } from "../../Network/Index";
import NewUserForm from '../../Forms/UserForm/NewUserForm';
import EditUserForm from "../../Forms/UserForm/EditUserForm";
import { getCookie } from "../../cookie/Index";
import Popup from "../../PopUp/PopUp";

export default function UserTabs() {
  const [displayIcon, setDisplayIcon] = useState(true);
  const [data, setData] = React.useState({});
  const [showPopup, setShowPopup] = useState(false);
  //  const [loading, setLoading] = useState(true);
  // variable to get data from store
  const currentState = useSelector((state) => state.userPageState);
  const userId = useSelector((state) => state.userIdChange);
  // variable to check the backend generated id of the user logged in
  const uniqueId = getCookie("UserId");
  // variable to check login user role
  const isAdmin = getCookie('isAdmin') === 'true';
  useEffect(() => {
    if (userId !== null) {
      const url = `/user/${userId}`;
      getData(url).then((res) => {
        setData(res.data.data);
      });
    }
  }, [userId]);
  // variable for dispatch an action
  const dispatch = useDispatch();

  // function called when edit icon is clicked
  // const handleEdit = (e) => {
  //   e.preventDefault();
  //   const url = `/user/${userId}`;
  //   getData(url).then((res) => {
  //     console.log(res);
  //     dispatch(saveUserInfo(res.data.data));
  //     setLoading(false);
  //   });

  // to show the edit user form
  //   if (!loading) {
  //     dispatch(userPageState("userEdit"));
  //   }
  // };
  const handleEdit = (e) => {
    e.preventDefault();
    // to show the edit role form
    dispatch(userPageState("userEdit"));
  };

  // function called when delete icon is clicked
  const handleDelete = () => {
    const url = `/user/${userId}`;
    // eslint-disable-next-line no-unused-vars
    deleteData(url).then((res) => {
      toast.error(res.data.message);
      // to show the details of first user in the list after deleting any user
      dispatch((userIdChange(null)));
      dispatch(reload());
      setShowPopup(false);
    }).catch((error) => {
      toast.error(error.response.data);
    });
  };

  return (
    <Paper elevation={3} className="clienttab">
      <TabContext value="1">
        <Box className="tabs">
          <TabList aria-label="lab API tabs example">
            <Tab label="Basic User Information" value="1" />
            <Box style={{ marginLeft: "22%" }}>
              {displayIcon && (uniqueId === data._id || isAdmin)
                ? <Button onClick={handleEdit} style={{ color: "white", padding: 12 }}><EditIcon /></Button>
              // eslint-disable-next-line react/jsx-no-useless-fragment
                : null }

              {displayIcon && (isAdmin)
                ? (
                  <Button
                    onClick={() => {
                      setShowPopup(true);
                    }}
                    style={{ color: "white", padding: 12 }}
                  >
                    {/* <Button onClick={handleDelete} style={{ color: "white", padding: 12 }}> */}
                    <DeleteIcon />
                  </Button>
                )
                // eslint-disable-next-line react/jsx-no-useless-fragment
                : null }

            </Box>
          </TabList>
        </Box>
        { currentState === "view" && (
          <Box style={{ height: "92%", overflowY: "scroll" }}>
            {!displayIcon && (setDisplayIcon(true))}
            <TabPanel value="1"><UsersInfoList /></TabPanel>

          </Box>
        )}
        { currentState === "create" && (
          <Box style={{ height: "92%", overflowY: "scroll" }}>
            {displayIcon && (setDisplayIcon(false))}
            <TabPanel value="1"><NewUserForm /></TabPanel>

          </Box>
        )}
        { currentState === "edit" && (
          <Box style={{ height: "92%", overflowY: "scroll" }}>
            {displayIcon && (setDisplayIcon(false))}
            <TabPanel value="1">
              <EditUserForm />
              {' '}
            </TabPanel>
          </Box>
        )}
      </TabContext>
      <ToastContainer />
      {showPopup ? (

        <Popup
          handleClose={() => { setShowPopup(false); }}
          handleDelete={() => { handleDelete(); }}
          handleCancel={() => { setShowPopup(false); }}
        />

      ) : null}
    </Paper>
  );
}
