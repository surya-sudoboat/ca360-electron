import React from 'react';
import { useSelector } from "react-redux";
import UsersHeading from './UsersHeading';
import UsersList from './UsersList';
import UsersTabs from './UsersTabs';

function UsersPage() {
  const userPageState = useSelector((state) => state.userPageState);

  return (
    <div className="rightSide">
      <div className="ClientSubheading">
        <UsersHeading />
      </div>
      <div className="clientContainer">
        <UsersList />
        {(userPageState === "view" || userPageState === "create" || userPageState === "edit")
          ? <UsersTabs /> : null}

      </div>
    </div>
  );
}

export default UsersPage;
