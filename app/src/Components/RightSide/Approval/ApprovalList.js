import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { Button, Typography, Paper } from "@material-ui/core";
import CircularProgress from '@mui/material/CircularProgress';
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Accordion from "@mui/material/Accordion";
import AccordionSummary from "@mui/material/AccordionSummary";
import AccordionDetails from "@mui/material/AccordionDetails";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import CheckCircleOutlineOutlinedIcon from '@mui/icons-material/CheckCircleOutlineOutlined';
import DoNotDisturbOutlinedIcon from '@mui/icons-material/DoNotDisturbOutlined';
import { ToastContainer, toast } from "react-toastify";
import { reload } from '../../../actions/index';
import {
  getData, putData, deleteData,
} from "../../Network/Index";

// Function for show Approvals List
export default function ApprovalList() {
  // Variable to strore client list
  const [clients, setClients] = useState([]);
  // variable for list of Approvals
  const [approvals, setApprovals] = useState([]);
  const [keys, setkeys] = useState([]);
  // variable for setting old values.
  const [oldData, setOldData] = useState([]);
  // variable for loading
  const [isLoading, setIsLoading] = useState(true);
  const [isLoadingOldData, setIsLoadingOldData] = useState(true);
  // variable for resfetch the approvals
  const refresh = useSelector((state) => state.reloadPage);
  const search = useSelector((state) => state.search);
  const searchItem = useSelector((state) => state.searchValue);
  // Variable to use dispatch
  const dispatch = useDispatch();
  // Variable for styling
  const listbtn = { color: "#ffffff", backgroundColor: "#8A2BE2" };
  const listbtnDeny = {
    color: "#00000080", backgroundColor: "white", marginLeft: "20px", border: "1px solid rgba(0, 0, 0, 0.5)",
  };
  const loaderStyle = { color: "#8A2BE2" };
  const [expanded, setExpanded] = useState(false);
  const listContent = {
    color: "#161616",
    textAlign: "left",
    fontFamily: "Helvetica",
    fontStyle: "normal",
    fontWeight: "400",
    fontSize: "13px",
  };
  const listTopic = {
    color: "#ffffff",
    textAlign: "left",
    backgroundColor: "#8A2BE2",
    fontFamily: "Helvetica",
    fontStyle: "normal",
    fontWeight: "700",
    fontSize: "13px",
    background: "#8A2BE2",
  };
  // Function to handle accordian
  const handleChange = (panel) => (event, isExpanded) => {
    setIsLoadingOldData(true);
    setExpanded(isExpanded ? panel : false);
  };

  useEffect(() => {
    const url = '/client';
    getData(url)
      .then((res) => {
        setClients(res.data.data);
      })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  }, []);

  // Function to get Approvals
  useEffect(() => {
    setIsLoading(true);
    const url = "/approval";
    getData(url).then((res) => {
      // eslint-disable-next-line no-plusplus
      for (let i = 0; i < res.data.data.length; i++) {
        // eslint-disable-next-line max-len
        res.data.data[i].clientName = clients.find((it) => it._id === res.data.data[i].requestingForClient)?.businessName;
        // eslint-disable-next-line max-len
        res.data.data[i].clientUniqueId = String(clients.find((it) => it._id === res.data.data[i].requestingForClient)?.uniqueId);
      }
      if (search) {
        setApprovals(res.data.data.filter((item) => item.approvalRequest?.includes(searchItem)
        || item.requestingUser.name?.includes(searchItem) || item.clientName?.includes(searchItem)
        || item.clientUniqueId?.includes(String(searchItem))));
      } else {
        setApprovals(res.data.data);
      }
      setIsLoading(false);
    })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  }, [refresh, clients, searchItem]);

  // function to handle approve request to approve
  const handleApprove = (id) => {
    const url = `/approval/update/${id}`;
    putData({}, url).then((res) => {
      toast.success(res.data.message);
      dispatch(reload());
    })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  };

  // function to handle approve request for client deletion
  const handleDeleteClientApprove = (id) => {
    const url = `/approval/delete/${id}`;
    deleteData(url).then((res) => {
      toast.success(res.data.message);
      dispatch(reload());
    })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  };

  // function to handle deny request
  const handleDeny = (id) => {
    const url = `/approval/${id}`;
    deleteData(url, {}).then((res) => {
      toast.error(res.data.message);
      dispatch(reload());
    })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  };

  // function to get old data
  const getOldInfo = (approval) => {
    const Clienturl = `/client/${approval.requestingForClient}`;
    getData(Clienturl).then((res) => {
      const oldValues = {
        ...res.data.data,
        dateOfBirth: res.data.data.dateOfBirth.substring(0, 10),
        startDate: res.data.data.startDate.substring(0, 10),
      };
      setOldData(oldValues);
      setkeys(Object.keys(oldValues));
      setIsLoadingOldData(false);
    })
      .catch((error) => {
        toast.error(error.response.data.message);
      });
  };

  // function to change camelCase to Title Case
  function unCamelCase(str) {
    return str
    // insert a space between lower & upper
      .replace(/([a-z])([A-Z])/g, '$1 $2')
    // space before last upper in a sequence followed by lower
      .replace(/\b([A-Z]+)([A-Z])([a-z])/, '$1 $2$3')
    // uppercase the first character
      .replace(/^./, (st) => st.toUpperCase());
  }
  return (

    <Paper elevation={3} className="listContainer">
      <ToastContainer />
      {/* Show loader untill data is fetch */}
      <div className="listTableContainer">
        <TableContainer style={{ maxHeight: "100%" }}>
          {(!isLoading && approvals.length === 0) ? <Typography variant="h4" style={{ textAlign: "center" }}>No record found!</Typography>
            : (
              <Table stickyHeader aria-label="a dense table">
                <TableHead style={{ height: "10px", color: "white" }}>
                  <TableRow>
                    <TableCell
                      style={{
                        color: "#ffffff",
                        textAlign: "left",
                        backgroundColor: "#8A2BE2",
                        fontFamily: "Helvetica",
                        fontStyle: "normal",
                        fontWeight: "700",
                        fontSize: "13px",
                        width: "80%",
                        background: "#8A2BE2",
                      }}
                    >
                      Description

                    </TableCell>
                    <TableCell style={listTopic}>
                      Approval State

                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {!isLoading ? (
                    <>
                      {approvals.map((approval, index = 1) => (
                        <TableRow>
                          <TableCell style={listContent}>
                            <div key={index}>

                              {approval.status === "open" && (
                              // Code for accordian of each approval
                              <Accordion
                                elevation={0}
                                expanded={expanded === `panel${index}`}
                                onChange={handleChange(`panel${index}`)}
                                style={{
                                  backgroundColor: "white", color: "black", margin: 4,
                                }}
                                onClick={() => { getOldInfo(approval); }}
                              >
                                <AccordionSummary
                                  expandIcon={<ExpandMoreIcon style={{ color: "black" }} />}
                                  aria-controls="panel1a-content"
                                  id="panel1a-header"
                                >
                                  <Typography>
                                    {approval.requestingUser.name}
                                    {" "}
                                    request for change to
                                    {" "}
                                    {unCamelCase(approval.approvalRequest)}
                                    {" "}
                                    for client Id
                                    {' '}
                                    {approval.data.uniqueId}
                                  </Typography>
                                </AccordionSummary>
                                {approval.approvalRequest === "updateClient" ? (
                                  <AccordionDetails style={{ textAlign: "left" }}>
                                    {/* Show loader until old data is fetch */}
                                    { (!isLoadingOldData && approval.data !== undefined) ? (
                                      keys.map((key, value = 0) => (
                                        <div key={value}>
                                          {(typeof (oldData[key])) === "object" ? (
                                            Object.keys(oldData[key]).map((it, val = 0) => (
                                              <div key={val}>
                                                {(it !== "_id" && approval.data[key] !== undefined && oldData[key][it] !== approval.data[key][it]) && (
                                                <Typography>
                                                  {approval.requestingUser.name}
                                                  { " request for change to " }
                                                  { `${unCamelCase(key)}-${unCamelCase(it)}: ${oldData[key][it]} => ${approval.data[key][it]}` }
                                                </Typography>
                                                )}
                                              </div>
                                            ))
                                          ) : (
                                            <div>
                                              {(key !== "_id" && oldData[key] !== approval.data[key]) && (
                                              <Typography>
                                                {approval.requestingUser.name}
                                                { " request for change to " }
                                                { ` ${unCamelCase(key)}: ${oldData[key]} => ${approval.data[key]}` }
                                              </Typography>
                                              )}
                                            </div>
                                          )}
                                        </div>
                                      ))) : (
                                        <div style={{ marginLeft: "30%" }}>
                                          <CircularProgress size={25} style={{ color: "white" }} />
                                        </div>
                                    )}

                                    {/* <div style={{ paddingLeft: "80%" }}>
                                  <div style={{ justifyContent: "space-between" }}>
                                    <Button
                                      style={listbtn}
                                      startIcon={(
                                        <CheckCircleOutlineOutlinedIcon />
                  )}
                                      onClick={() => { handleApprove(approval._id); }}
                                    >
                                      Approve
                                    </Button>
                                    <Button
                                      style={listbtnDeny}
                                      startIcon={(
                                        <DoNotDisturbOutlinedIcon />
                  )}
                                      onClick={() => { handleDeny(approval._id); }}
                                    >
                                      Deny
                                    </Button>
                                  </div>
                                </div> */}
                                  </AccordionDetails>
                                ) : (
                                  <AccordionDetails>
                                    <div style={{ paddingLeft: "80%" }}>
                                      <div style={{ justifyContent: "space-between" }}>
                                        <Button
                                          style={listbtn}
                                          startIcon={(
                                            <CheckCircleOutlineOutlinedIcon />
                )}
                                          onClick={() => {
                                            handleDeleteClientApprove(approval._id);
                                          }}
                                        >
                                          <Typography style={{
                                            fontFamily: "Helvetica",
                                            fontStyle: "normal",
                                            fontWeight: "700",
                                            fontSize: "13px",
                                            textTransform: "none",
                                          }}
                                          >
                                            {' '}
                                            Approve

                                          </Typography>

                                        </Button>
                                        <Button
                                          style={listbtnDeny}
                                          startIcon={(
                                            <DoNotDisturbOutlinedIcon />
                )}
                                          onClick={() => { handleDeny(approval._id); }}
                                        >

                                          <Typography style={{
                                            fontFamily: "Helvetica",
                                            fontStyle: "normal",
                                            fontWeight: "700",
                                            fontSize: "13px",
                                            textTransform: "none",
                                          }}
                                          >
                                            {' '}
                                            Deny

                                          </Typography>
                                        </Button>
                                      </div>
                                    </div>
                                  </AccordionDetails>
                                )}
                              </Accordion>
                              )}

                            </div>
                          </TableCell>

                          <TableCell
                            style={{
                              color: "#161616",
                              textAlign: "center",
                              fontFamily: "Helvetica",
                              fontStyle: "normal",
                              fontWeight: "400",
                              fontSize: "13px",
                            }}
                          >
                            <div>
                              <div style={{ display: "flex", justifyContent: "space-between" }}>
                                <Button
                                  style={listbtn}
                                  startIcon={(
                                    <CheckCircleOutlineOutlinedIcon />
                  )}
                                  onClick={() => { handleApprove(approval._id); }}
                                >
                                  <Typography style={{
                                    fontFamily: "Helvetica",
                                    fontStyle: "regular",
                                    fontWeight: "400",
                                    fontSize: "13px",
                                    textTransform: "none",
                                  }}
                                  >
                                    {' '}
                                    Approve

                                  </Typography>
                                </Button>
                                <Button
                                  style={listbtnDeny}
                                  startIcon={(
                                    <DoNotDisturbOutlinedIcon />
                  )}
                                  onClick={() => { handleDeny(approval._id); }}
                                >
                                  <Typography style={{
                                    fontFamily: "Helvetica",
                                    fontStyle: "regular",
                                    fontWeight: "400",
                                    fontSize: "13px",
                                    textTransform: "none",
                                  }}
                                  >
                                    {' '}
                                    Deny

                                  </Typography>
                                </Button>
                              </div>
                            </div>
                          </TableCell>
                        </TableRow>
                      ))}
                    </>
                  ) : (
                    <div className="approvalLoader">
                      <CircularProgress size={50} style={loaderStyle} />
                    </div>
                  )}
                </TableBody>

              </Table>
            )}

        </TableContainer>
      </div>

    </Paper>
  //   <Paper elevation={3} className="listContainer">
  //   {/* <Typography variant="h4" className="listHeading">
  //     List of Clients
  //     <FormControlLabel
  //       style={{ float: 'right' }}
  //       control={(
  //         <Switch
  //           checked={gstState}
  //           onChange={(e) => { setGstState(e.target.checked); }}
  //         />
  //           )}
  //       label="Show GST Clients"
  //     />

  //   </Typography>
  //    */}
  //   {/* Wait Untill get response */}
  //   {/*  {(!isLoading) ? (  */}

  //   <div className="listTableContainer">
  //     <TableContainer style={{ maxHeight: "100%" }}>
  //       <Table stickyHeader aria-label="a dense table">
  //         {/* table headings */}
  //         <TableHead style={{ height: "10px", color: "white" }}>
  //           <TableRow>
  //             <TableCell style={listTopic}>S.No</TableCell>
  //             <TableCell style={listTopic}>Business Name</TableCell>
  //             <TableCell style={listTopic}>Person Name</TableCell>
  //             <TableCell style={listTopic}>Primary Mobile Number</TableCell>
  // eslint-disable-next-line max-len
  //             {(currentState !== "viewClient" && currentState !== "createClient" && currentState !== "editClient") ? <TableCell style={listTopic}>Actions</TableCell> : null }
  //             <TableCell style={listTopic} />
  //           </TableRow>
  //         </TableHead>
  //         {/* table data */}
  //         {!isLoading ? (
  //           <TableBody style={{ overflowY: "scroll" }}>
  //             {list.map((row, index = 1) => (
  //               <TableRow
  //                 key={index}
  //                 style={{ cursor: "pointer" }}
  //                 onClick={() => {
  //                   handleView(row._id);
  //                 }}
  //               >
  //                 <TableCell style={listContent}>
  //                   <div style={{ display: "flex" }}>
  //                     {index + 1}
  //                     {" "}
  //                     {!gstState && row.gst ? (
  //                       <img
  //                         style={gstIconStyle}
  //                         src={gstIcon}
  //                         alt="gstIcon"
  //                       />
  //                     ) : null}
  //                   </div>
  //                 </TableCell>
  //                 <TableCell style={listContent}>
  //                   {row.businessName}
  //                 </TableCell>
  //                 <TableCell style={listContent}>{row.dependent}</TableCell>
  //                 <TableCell style={listContent}>
  //                   {row.contacts.primaryMobile}
  //                 </TableCell>
  // eslint-disable-next-line max-len
  //                 {(currentState !== "viewClient" && currentState !== "createClient" && currentState !== "editClient")
  //                   ? (
  //                     <TableCell style={listContent}>

  //                       <EditIcon
  //                         onClick={(e) => {
  //                           handleEdit(row._id);
  //                         }}
  //                         style={{ pointer: "cursor", color: "#7E7E7E" }}
  //                       />

  //                       <DeleteIcon
  //                         onClick={(e) => {
  //                           e.stopPropagation();
  //                           setShowPopup(true);
  //                           dispatch(clientIDchange(row._id));
  //                           //   handleDelete();
  //                         }}
  //                         style={{ marginLeft: "20px", pointer: "cursor", color: "#7E7E7E" }}
  //                       />

  //                     </TableCell>
  //                   ) : null }
  //                 <TableCell>
  //                   {/*  <Button
  //                     style={listbtn}
  //                     onClick={() => {
  //                       handleView(row._id);
  //                     }}
  //                   >
  //                     View Detail
  //                   </Button>
  //     */}
  //                   {" "}
  //                 </TableCell>
  //               </TableRow>
  //             ))}
  //           </TableBody>
  //         ) : (
  //           <TableBody>
  //             <TableRow>
  //               <TableCell colSpan={5} style={{ borderBottom: "none" }}>
  //                 <CircularProgress
  //                   size={50}
  //                   style={{ margin: "16% 45%", color: "#8A2BE2" }}
  //                 />
  //               </TableCell>
  //             </TableRow>
  //           </TableBody>
  //         )}
  //       </Table>
  //     </TableContainer>
  //     <ToastContainer />
  //     {showPopup ? (
  //       <>
  //         {/*
  //       <div className="popup-box">
  //         <div className="box">
  //           <span className="close-icon">
  //           <CloseIcon onClick={() => { setShowPopup(false); }} />
  //           </span>
  //           <h4>Are you sure to delete this file?</h4>
  //           <h6>If you delete this file you can;t recover it.</h6>
  //           <div style={{ display: "flex", marginLeft: "50%" }}>
  //             <Button onClick={() => { setShowPopup(false); }}>Cancel</Button>
  //             <Button onClick={() => {
  //               // setDeleteItem(true);
  //               // console.log(`button${deleteItem}`);
  //               handleDelete();
  //             }}
  //             >
  //               Delete
  //             </Button>
  //           </div>
  //         </div>
  //         </div>
  //           */}
  //         <Popup
  //           handleClose={() => { setShowPopup(false); }}
  //           handleDelete={() => { handleDelete(); }}
  //           handleCancel={() => { setShowPopup(false); }}
  //         />

  //       </>
  //     ) : null}

  //   </div>
  //   {/*   ) : (
  //     <div className="leftLoader">
  //       <CircularProgress size={50} style={loaderStyle} />
  //     </div>
  // )}  */}
  // </Paper>
  );
}
