import React from 'react';
import ApprovalHeading from "./ApprovalHeading";
import ApprovalList from "./ApprovalList";

// Function for show Approvals Page
function ApprovalsPage() {
  return (
    <div className="rightSide" style={{ marginRight: "1%" }}>
      <div className="clientSubheading">
        <ApprovalHeading />
      </div>
      <div className="approvalList">
        <ApprovalList />
      </div>
    </div>
  );
}

export default ApprovalsPage;
