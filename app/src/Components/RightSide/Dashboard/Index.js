import React from 'react';
import DashboardHeading from "./DashboardHeading";
// Function for show Approvals Page
function ApprovalsPage() {
  return (
    <div className="rightSide">
      <div className="clientSubheading">
        <DashboardHeading />
      </div>
    </div>
  );
}

export default ApprovalsPage;
