import React from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import {
  Typography, List, ListItem, ListItemText, Box, //  Divider,
} from '@material-ui/core';
import LogoutIcon from '@mui/icons-material/Logout';
import logoImage from "../../Images/logo.png";
import {
  userPageState, viewClient, reload, ResetStore,
  search, //  clientIDchange,   roleIdChange, rolePageState,
} from "../../actions/index";
import { deleteCookie, getCookie } from "../cookie/Index";

export default function LeftNavBar() {
  // Variable for dispatch
  const dispatch = useDispatch();
  // Variable to fectech role of user
  const isAdmin = getCookie('isAdmin') === 'true';

  // Function To handle Logout
  const handleLogout = () => {
    // Reset the store
    dispatch(ResetStore());
    // Delete the cookies
    deleteCookie(`token=${`${getCookie("token")};max-age=0`}`);
    deleteCookie(`UserId=${`${getCookie("UserId")};max-age=0`}`);
    deleteCookie(`isAdmin=${`${getCookie("isAdmin")};max-age=0`}`);
  };
  return (
    // Left Nav Bar
    <div className="leftNavbar">
      <Box
        style={{ margin: "20px 15px 10px 15px", width: "70%", height: "15%" }}
        component="img"
        src={logoImage}
        alt="person"
      />
      {/* On every Click Current page status Changes */}
      <List>
        <Link to="/dashboard" className="leftItems">
          <ListItem button>
            <ListItemText>
              <Typography style={{ fontSize: "18px", fontWeight: "400" }}>Dashboard </Typography>
            </ListItemText>
          </ListItem>
        </Link>

        <Link to="/clients" className="leftItems" onClick={() => { dispatch(viewClient(null)); dispatch(search(false)); }}>
          {' '}
          {/* dispatch(viewClient()); dispatch(clientIDchange('1'));  dispatch(reload()); */}
          <ListItem button>
            <ListItemText>
              <Typography style={{ fontSize: "18px", fontWeight: "400" }}>Clients </Typography>
            </ListItemText>
          </ListItem>
        </Link>
        <Link to="/users" className="leftItems" onClick={() => { dispatch(userPageState("userView", null)); dispatch(search(false)); }}>
          <ListItem button>
            <ListItemText>
              <Typography style={{ fontSize: "18px", fontWeight: "400" }}>Users </Typography>
            </ListItemText>
          </ListItem>
        </Link>
        {/* {isAdmin
        && (
        <Link to="/roles" className="leftItems"
         onClick={() => { dispatch(rolePageState("roleView", null)); dispatch(reload()); }}>
          <ListItem button>
            <ListItemText>
              <Typography style={{ fontSize: "18px", fontWeight: "400" }}>Roles </Typography>
            </ListItemText>
          </ListItem>
        </Link>
        )} */}
        {isAdmin && (
        <Link to="/approval" className="leftItems" onClick={() => { dispatch(search(false)); }}>
          <ListItem button>
            <ListItemText>
              <Typography style={{ fontSize: "18px", fontWeight: "400" }}>Approval </Typography>
            </ListItemText>
          </ListItem>
        </Link>
        )}
        <Link to="/taskmanagement" className="leftItems" onClick={() => { dispatch(search(false)); }}>
          <ListItem button>
            <ListItemText>
              <Typography style={{ fontSize: "18px", fontWeight: "400" }}>Task Management </Typography>
            </ListItemText>
          </ListItem>
        </Link>
      </List>
      <Link
        to="/"
        onClick={handleLogout}
        style={{
          textDecoration: "none", color: "white", position: "absolute", bottom: "30px",
        }}
      >
        <Typography style={{ fontSize: "18px", fontWeight: "400" }}>
          <LogoutIcon style={{ margin: "-5px 10px" }} />
          Log Out
        </Typography>
      </Link>
    </div>
  );
}
