import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import {
  Grid, Box, TextField, Button, Typography, Link, IconButton,
} from '@material-ui/core';
import Divider from "@mui/material/Divider";
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import InputAdornment from '@material-ui/core/InputAdornment';
import DraftsOutlinedIcon from '@mui/icons-material/DraftsOutlined';
import { ToastContainer, toast } from 'react-toastify';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Visibility from '@mui/icons-material/Visibility';
import { loginPageStyles } from "../../css/index";
import logoImage from "../../Images/logo.png";
import { postData } from '../Network/Index';
import { setCookie } from "../cookie/Index";

function Loginpage() {
  // Variables for mui Styles
  const classes = loginPageStyles();
  // Variables to store the state of the form.
  const navigate = useNavigate();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  // Var and Function to show/hide the password.
  // const [showPassword] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  // function to make the password visible or not visible depending on the icon clicked.
  // function passwordVisibility() {
  //   setShowPassword(!showPassword);
  // }

  // Function to handle the Login button.

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = { username: email, password };
    const url = '/auth/login';
    postData(data, url)
      .then((res) => {
        if (res.status === 200) {
          // Set the cookies
          setCookie("token", res.data.data.accessToken, 1);
          setCookie("UserId", res.data.data._id, 1);
          if (res.data.data.role === "Admin") {
            setCookie("isAdmin", true, 1);
          } else {
            setCookie("isAdmin", false, 1);
          }
          navigate(`/clients`);
        }
      })
      .catch((error) => {
        toast.error(error.response.data.message, {
          position: toast.POSITION.TOP_CENTER,
        });
      });
  };

  return (
    <Box className={classes.container}>
      <Box className={classes.leftSide}>
        <Box
          className={classes.logo}
          component="img"
          src={logoImage}
          alt="person"
        />
      </Box>
      <Box className={classes.rightSide}>
        <Box className={classes.loginContainer}>
          <form onSubmit={handleSubmit}>
            <Grid container direction="column" spacing={2}>

              <Grid item>
                <Box className={classes.heading}>
                  <Typography variant="h5">
                    Login to Account
                  </Typography>
                  <Divider className={classes.line} style={{ margin: "10px auto" }} />
                </Box>
              </Grid>
              <Grid item>
                <Typography variant="h6">Username</Typography>
                <TextField
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                  type="email"
                  value={email}
                  variant="outlined"
                  placeholder="Enter Username"
                  fullWidth
                  required
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <DraftsOutlinedIcon />
                      </InputAdornment>
                    ),
                  }}
                />
              </Grid>
              <Grid item>
                <Typography variant="h6">Password</Typography>
                <TextField
                  onChange={(e) => {
                    setPassword(e.target.value);
                  }}
                  type={showPassword ? 'text' : 'password'}
                  value={password}
                  variant="outlined"
                  placeholder="Enter password"
                  fullWidth
                  required
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <LockOutlinedIcon />
                      </InputAdornment>
                    ),
                    endAdornment: (
                      <InputAdornment position="end">

                        <IconButton onClick={() => { setShowPassword(!showPassword); }}>
                          {showPassword ? <Visibility />
                            : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    ),

                  }}
                />
              </Grid>
              <Grid item>
                <Button
                  type="submit"
                  style={{ color: "white", background: "#8A2BE2" }}
                  variant="contained"
                  fullWidth
                >
                  Log in
                </Button>
              </Grid>
              <Grid item>
                <Link href="none" underline="hover" className={classes.forgetColor}>
                  <Typography variant="h6" align="center">
                    Forget password?
                  </Typography>
                </Link>
              </Grid>
            </Grid>
          </form>
        </Box>
      </Box>
      <ToastContainer />
    </Box>
  );
}

export default Loginpage;
