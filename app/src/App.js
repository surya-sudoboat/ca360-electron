import React from 'react';
import { Routes, Route } from 'react-router-dom';
// import { Typography } from '@material-ui/core';
import LoginPage from './Components/Loginpage/Index';
// import Header from './Components/Header/Index';
import LeftNavBar from './Components/LeftNavBar/Index';
import DashboardPage from './Components/RightSide/Dashboard/Index';
import ClientsPage from './Components/RightSide/Clients/Index';
import RolesPage from './Components/RightSide/Roles/Index';
import TasksPage from './Components/RightSide/TaskManagement/Index';
import UsersPage from './Components/RightSide/Users/Index';
import ApprovalPage from './Components/RightSide/Approval/Index';
import { PrivateRoute, CheckRoute } from './PrivateRoute';
import './style.css';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <Routes>
      <Route
        path="/"
        element={(
          <CheckRoute>
            <LoginPage />
          </CheckRoute>
        )}
      />
      <Route
        path="/dashboard"
        element={(
          <PrivateRoute>
            <div className="homeContainer">
              {/*  <Header /> */}
              <div className="downContainer">
                <LeftNavBar />
                <DashboardPage />
              </div>
            </div>
          </PrivateRoute>
        )}
      />
      <Route
        path="/clients"
        element={(
          <PrivateRoute>
            <div className="homeContainer">
              {/*  <Header /> */}
              <div className="downContainer">
                <LeftNavBar />
                <ClientsPage />
              </div>
            </div>
          </PrivateRoute>
        )}
      />
      <Route
        path="/users"
        element={(
          <PrivateRoute>
            <div className="homeContainer">
              {/*  <Header /> */}
              <div className="downContainer">
                <LeftNavBar />
                <UsersPage />
              </div>
            </div>
          </PrivateRoute>
        )}
      />
      <Route
        path="/roles"
        element={(
          <PrivateRoute>
            <div className="homeContainer">
              {/*  <Header /> */}
              <div className="downContainer">
                <LeftNavBar />
                <RolesPage />
              </div>
            </div>
          </PrivateRoute>
        )}
      />
      <Route
        path="/approval"
        element={(
          <PrivateRoute>
            <div className="homeContainer">
              {/*  <Header /> */}
              <div className="downContainer">
                <LeftNavBar />
                <ApprovalPage />
              </div>
            </div>
          </PrivateRoute>
        )}
      />

      <Route
        path="/taskmanagement"
        element={(
          <PrivateRoute>
            <div className="homeContainer">
              {/*  <Header /> */}
              <div className="downContainer">
                <LeftNavBar />
                <TasksPage />
              </div>
            </div>
          </PrivateRoute>
        )}
      />
    </Routes>
  );
}

export default App;
