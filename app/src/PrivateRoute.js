import React from 'react';
import { Navigate } from 'react-router-dom';
import { getCookie } from "./Components/cookie/Index";

// Function to check if user  previous logged in untill cokkies expires or not
// If user is not logged in then redirect to login page
function PrivateRoute({ children }) {
  const auth = getCookie("token");
  return auth ? children : <Navigate to="/" />;
}

function CheckRoute({ children }) {
  const auth = getCookie("token");
  return auth ? <Navigate to="/clients" /> : children;
}
export { PrivateRoute, CheckRoute };
