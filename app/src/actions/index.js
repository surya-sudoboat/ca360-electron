// Actions of redux store used to update store

// This hadle to view create new client page in tabs.
export const createClient = (payload) => ({
  type: 'createClient',
  payload,
});

// This hadle to view edit client page in tabs.
export const editClient = () => ({
  type: 'editClient',
});

// This hadle to view client information page in tabs.
// export const viewClient = () => ({
//   type: 'viewClient',
// });

export const viewClient = (payload) => ({
  type: 'viewClient',
  payload,
});

export const userPageState = (state, payload) => (
  {
    type: state,
    payload,
  }
);
// This hadle to change user id in store for view information of user.
export const userIdChange = (userId) => (
  {
    type: "userIdChange",
    payload: userId,
  }
);
// This hadle to change client id in store for view information of client.
export const clientIDchange = (clientID) => ({
  type: 'clientIDchange',
  payload: clientID,
});

export const roleIdChange = (roleId) => (
  {
    type: "roleIdChange",
    payload: roleId,
  }
);

export const approvalIdChange = (approvalId) => (
  {
    type: "approvalIdChange",
    payload: approvalId,
  }
);

export const rolePageState = (state, payload) => (
  {
    type: state,
    payload,
  }
);

// This hadle to reload data from API.
export const reload = () => ({
  type: 'reload',
});

// This hadle to reset the data of client form.
export const resetClinetInfo = () => ({
  type: 'resetClinetInfo',
});

// This hadle to save the data of client form in redux store.
export const saveClinetInfo = (clientInfo) => ({
  type: 'saveClinetInfo',
  payload: clientInfo,
});

// This hadle to reset the data of user form.
// export const resetUserInfo = () => ({
//   type: 'resetUserInfo',
// });

// This hadle to save the data of user form in redux store.
// export const saveUserInfo = (userInfo) => ({
//   type: 'saveUserInfo',
//   payload: userInfo,
// });

// This hadle to change tabs of client page.
export const ChangeTab = (value) => ({
  type: 'ChangeTab',
  payload: value,
});

// This hadle to reset the data of task form.
export const resetTaskInfo = () => ({
  type: 'resetTaskInfo',
});

// This hadle to save the data of task form in redux store.
export const saveTaskInfo = (clientInfo) => ({
  type: 'saveTaskInfo',
  payload: clientInfo,
});

// This hadle to set the id of task in store.
export const setTaskId = (taskId) => ({
  type: 'setTaskId',
  payload: taskId,
});

// This hadle to set reset store on Logout
export const ResetStore = () => ({
  type: 'USER_LOGOUT',
});

export const showGst = (boolean) => ({
  type: "showGst",
  payload: boolean,
});

export const searchValue = (arg) => ({
  type: "searchValue",
  payload: arg,
});

export const search = (boolean) => ({
  type: "search",
  payload: boolean,
});
