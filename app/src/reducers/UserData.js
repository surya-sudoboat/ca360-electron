const initialState = {
  name: "",
  username: "",
  password: "",
};

// eslint-disable-next-line default-param-last
export default function userData(state = initialState, action) {
  switch (action.type) {
    case "saveUserInfo": {
      state = action.payload;
      return state;
    }
    case "resetUserInfo": {
      state = initialState;
      return state;
    }
    default: {
      return state;
    }
  }
}
