/* eslint-disable default-param-last */
// This function is used to handle the tab change of client page.

const value = '1';
const CurrentTab = (state = value, action) => {
  switch (action.type) {
    case 'ChangeTab':
      state = action.payload;
      return state;
    default:
      return state;
  }
};

export { CurrentTab };
