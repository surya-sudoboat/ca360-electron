/* eslint-disable max-len */
/* eslint-disable default-param-last */

// This handle the client page mode whether it is createClient, editClient or viewClient.
// const initialState = 'viewClient';
const initialState = null;
const clientPageState = (state = initialState, action) => {
  switch (action.type) {
    case 'createClient':
      if (action.payload === null) {
        state = null;
      } else {
        state = 'createClient';
      }
      return state;
      /*  case 'editClient':
      state = 'editClient';
      return state; */
    case 'editClient':
      if (action.payload === null) {
        state = null;
      } else {
        state = 'editClient';
      }
      return state;
      /*
      case 'viewClient':
      state = 'viewClient';
      return state;
       */
    case 'viewClient':
      if (action.payload === null) {
        state = null;
      } else {
        state = 'viewClient';
      }
      return state;
    default:

      return state;
  }
};

// This handle the user page mode whether it is createUser, editUser or viewUser.
// const initialUserPage = "view";
const initialUserPage = null;
const userPageState = (state = initialUserPage, action) => {
  switch (action.type) {
    case 'userCreate':
      if (action.payload === null) {
        state = null;
      } else {
        state = "create";
      }

      return state;
      /*  case 'userEdit':
      state = "edit";
      return state; */

    case 'userEdit':
      if (action.payload === null) {
        state = null;
      } else {
        state = "edit";
      }
      return state;

    /* case 'userView':
       state = "view";
       return state;  */
    case 'userView':
      if (action.payload === null) {
        state = null;
      } else {
        state = "view";
      }
      return state;
    default:

      return state;
  }
};

// This function is used to update clientID in store for view information of client.
const clientID = '1';

const clientIDchange = (state = clientID, action) => {
  switch (action.type) {
    case 'clientIDchange':
      state = action.payload;
      return state;
    default:

      return state;
  }
};

// This function is used reload data from API.
const reload = true;
const reloadPage = (state = reload, action) => {
  switch (action.type) {
    case 'reload':
      return !state;
    default:

      return state;
  }
};

// This function is used to update userID in store for view information of user.
const userId = null;
const userIdChange = (state = userId, action) => {
  switch (action.type) {
    case "userIdChange":
      state = action.payload;
      return state;

    default:

      return state;
  }
};

// This function is used to update roleID in store for view information of role.
const roleId = null;
const roleIdChange = (state = roleId, action) => {
  switch (action.type) {
    case "roleIdChange":
      state = action.payload;
      return state;

    default:

      return state;
  }
};

// This function is used to update userID in store for view information of user.
const approvalId = null;
const approvalIdChange = (state = approvalId, action) => {
  switch (action.type) {
    case "approvalIdChange":
      state = action.payload;
      return state;

    default:

      return state;
  }
};
// This handle the role page mode whether it is createRole, editRole or viewRole.
// const initialRolePage = "view";
const initialRolePage = null;
const rolePageState = (state = initialRolePage, action) => {
  switch (action.type) {
    case 'roleCreate':
      if (action.payload === null) {
        state = null;
      } else {
        state = "create";
      }

      return state;
      /* case 'roleEdit':
      state = "edit";
      return state;    */

    case 'roleEdit':
      if (action.payload === null) {
        state = null;
      } else {
        state = "edit";
      }
      return state;

    case 'roleView':
      if (action.payload === null) {
        state = null;
      } else {
        state = "view";
      }
      return state;
    default:

      return state;
  }
};

const initial = false;
const showGst = (state = initial, action) => {
  switch (action.type) {
    case "showGst":
      state = action.payload;
      return state;
    default:
      return state;
  }
};

const initialSearchValue = null;
const searchValue = (state = initialSearchValue, action) => {
  switch (action.type) {
    case "searchValue":
      state = action.payload;
      return state;
    default:
      return state;
  }
};

const initialSearch = false;
const search = (state = initialSearch, action) => {
  switch (action.type) {
    case "search":
      state = action.payload;
      return state;
    default:
      return state;
  }
};
export {
  userIdChange, clientPageState, roleIdChange, clientIDchange, reloadPage, userPageState, rolePageState, approvalIdChange, showGst, searchValue, search,
};
