// Here all the reducers are combined and exported to be used in the store

import { combineReducers } from 'redux';
import {
  clientPageState,
  clientIDchange,
  reloadPage,
  userIdChange,
  roleIdChange,
  userPageState,
  rolePageState,
  approvalIdChange,
  showGst,
  searchValue,
  search,
} from './reducers';
import { NewClientData } from './CreateClientData';
import { CurrentTab } from './HandleTabs';
import { NewTaskData, selectedTaskId } from './taskData';
import userData from "./UserData";

const appReducer = combineReducers({
  clientPageState,
  clientIDchange,
  reloadPage,
  NewClientData,
  CurrentTab,
  NewTaskData,
  userIdChange,
  userPageState,
  selectedTaskId,
  rolePageState,
  roleIdChange,
  approvalIdChange,
  userData,
  showGst,
  searchValue,
  search,
});

// reset store if user logout
const rootReducer = (state, action) => {
  if (action.type === 'USER_LOGOUT') {
    return appReducer(undefined, action);
  }

  return appReducer(state, action);
};

export default rootReducer;
