/* eslint-disable default-param-last */
// Variable used for task form
const initialValues = {
  _id: '',
  taskName: '',
  assignedTo: '',
  description: '',
  uploadFile: '',
};

// This function is used to update the data of task in store with new data.
const NewTaskData = (state = initialValues, action) => {
  switch (action.type) {
    case 'saveTaskInfo':
      state = action.payload;
      return state;
    case 'resetTaskInfo':
      return initialValues;
    default:
      return state;
  }
};

const id = "";
const selectedTaskId = (state = id, action) => {
  switch (action.type) {
    case 'setTaskId':
      state = action.payload;
      return state;
    default:
      return state;
  }
};

export { NewTaskData, selectedTaskId };
