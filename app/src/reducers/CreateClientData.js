/* eslint-disable default-param-last */
// Values used for client form
const initialValues = {
  businessName: '',
  dependent: '',
  dateOfBirth: '',
  startDate: '',
  aadhaar: '',
  entityType: '',
  natureOfBusiness: [],
  notes: '',
  contacts: {
    primaryMobile: '+91 ',
    secondaryMobile: '+91 ',
    primaryMail: '',
    secondaryMail: '',
    address: '',
    whatsappTo: '',
    whatsappGroupType: '',
  },
  it: {
    username: '',
    password: '',
  },
  gst: {
    gstNumber: '',
    gstUsername: '',
    gstPassword: '',
    ewayUsername: '',
    ewayPassword: '',
    filingType: '',
    registeredState: '',
    verificationCode: '',
    paymentMode: '',
  },
  tds: {
    tanNumber: '',
    itUsername: '',
    tracesUsername: '',
    tracesPassword: '',
    itPassword: '',
  },
};

// This function is used to update the data of client in store with new data.
const NewClientData = (state = initialValues, action) => {
  switch (action.type) {
    case 'saveClinetInfo':
      state = action.payload;
      return state;
    case 'resetClinetInfo':
      return initialValues;
    default:
      return state;
  }
};

export { NewClientData };
