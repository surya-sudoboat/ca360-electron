import { makeStyles } from '@material-ui/core';

export const loginPageStyles = makeStyles({
  container: {
    height: "100vh", width: "100%", display: "flex",
  },
  leftSide: {
    width: "24%", height: "100%", textAlign: "center", background: "#8A2BE2",
  },
  logo: {
    position: "absolute", top: "45%", transform: "translate(-50%, -50%)", width: "12%",
  },
  rightSide: { width: "76%", height: "100" },
  loginContainer: {
    width: "30%", justifyContent: "center", margin: "auto", position: "relative", top: "23%",
  },
  heading: { textAlign: 'center', fontWeight: 'bold' },
  line: {
    padding: "1px", background: "black", width: "60%",
  },
  forgetColor: { color: "black" },
});
