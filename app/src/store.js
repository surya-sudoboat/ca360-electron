/* eslint-disable import/no-named-as-default */
import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import rootreducer from './reducers/index';

const store = createStore(rootreducer, composeWithDevTools());

export default store;
